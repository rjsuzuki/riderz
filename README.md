# Riderz (BART) 🚇 a transportation app

[Riderz Website and Documentation](https://rjsuzuki.gitlab.io/riderz/)

[![pipeline status](https://gitlab.com/rjsuzuki/riderz/badges/master/pipeline.svg)](https://gitlab.com/rjsuzuki/riderz/-/commits/master)
[![coverage report](https://gitlab.com/rjsuzuki/riderz/badges/master/coverage.svg)](https://gitlab.com/rjsuzuki/riderz/-/commits/master)
--------------------------------------------------------------------------------
## Screenshots

![home_night](https://i.imgur.com/nyBEf0Pm.png)
![home_day](https://i.imgur.com/qHOykRLm.png)
![nav](https://i.imgur.com/QRMdt7mm.png)
![fab](https://i.imgur.com/ee8URSAm.png)
![trip](https://i.imgur.com/2vu1pHfm.png)
![results](https://i.imgur.com/oZOP63tm.png)
![favorites](https://i.imgur.com/SeacQugm.png)
![gmap](https://i.imgur.com/Jt7od97m.png)
![bartmap](https://i.imgur.com/sEL3HuXm.png)

## About

An app made to help people navigate the BART Transit system in Northern California (including myself).

## Localization

- 🇺🇸


## Linting, Styling, & Documentation
- [dokka](https://github.com/Kotlin/dokka/blob/master/README.md)
- `./gradlew dokkaHtml`
- `./gradlew spotlessCheck`
- `./gradlew spotlessApply`

### Linking Gitlab Issues
For commits, use `#123`

## License 
Refer to the LICENSE.md file for licensing details.
