# Riderz Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioniklklng](https://semver.org/spec/v2.0.0.html).

### Types of changes:
- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

---------------------------------------------------

## [Release]

## [3.2.0] - 2021-03-21
### Fixed
- minor bug fixes
- fixed a crash when trying to request location permission for the first time
- minor UI fixes

### Changed
- improved location permission & service management
- splash background & minor adjustments to the navigation ui

### Added
- more Accessibility support
- new train station icons
- dokka for javadoc/ktdoc documentation support
- spotless for automated linting
- implemented groundwork for Google Play's In-app Review library

---------------------------------------------------
## [Unreleased]

## [3.1.1] - 2021-2-24
### Fixed
- minor UI bug fixes on Home screen

---------------------------------------------------
## [Release]

## [3.1.0] - 2021-02-26
### Added
- Beginning of new Changelog

### Fixed
- fixed NPEs
- fixed recyclerview race condition
