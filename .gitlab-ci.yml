image: openjdk:11-jdk

variables:
  ANDROID_COMPILE_SDK: "31"
  ANDROID_BUILD_TOOLS: "31.0.0"
  ANDROID_SDK_TOOLS: "6858069_latest"

before_script:
  - echo $PWD # where am I?
  - apt-get --quiet update --yes
  - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1 ruby-full build-essential g++
  - wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}.zip
  - unzip -d android-sdk-linux android-sdk.zip
  - export ANDROID_HOME=$PWD/android-sdk-linux
  - echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses
  - android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/nul
  - echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null

  - export PATH=$PATH:$ANDROID_HOME/platform-tools/
  - chmod +x ./gradlew
  # This is important to temporarily disable checking for EPIPE error and use yes to accept all licenses
  - set +o pipefail
  - echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses
  - set -o pipefail

  # This step installs Fastlane
  - gem install bundler
  - bundle install

  - "export VERSION_CODE=$((1 + $CI_PIPELINE_IID)) && echo $VERSION_CODE"
  - "export VERSION_SHA=`echo ${CI_COMMIT_SHORT_SHA}` && echo $VERSION_SHA"
  - filename=version_30200
  - suffix="${filename##*[0-9]}"
  - number="${filename%"$suffix"}"
  - number="${number##*[!-0-9]}"

# This step specifies the stages to be executed when the script is running. For this example, I used only one stage.
# You can have more than one stage listed here.
stages:
  - lint
  - build
  - test
  - deploy

lintDebug:
  stage: lint
  script:
    - ./gradlew -Pci --console=plain spotlessApply
    #- ./gradlew -Pci --console=plain :app:lintDebug -PbuildDir=lint
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master


assembleDebug:
  stage: build
  script:
    - ./gradlew assembleDebug
  artifacts:
    paths:
      - app/build/outputs/
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master


debugTests:
  stage: test
  script:
    - ./gradlew -Pci --console=plain :app:testDebug
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master


  # This step defines where the release apk is uploaded to on Google Play Store.
  # In this example, it's uploaded to the Beta track/phase when the pipeline in my master branch (on Gitlab) runs successfully.

signingConfig:
  stage: deploy
  only:
    - master
  script:
    - echo $RIDERZ_STORE_FILE | base64 -d > riderz-release-key.keystore
  artifacts:
      paths:
        - riderz-release-key.keystore
      expire_in: 10 mins

assembleRelease:
  stage: deploy
  only:
    - master
  script:
    - echo $RIDERZ_STORE_FILE | base64 -d > riderz-release-key.keystore
    - ./gradlew assembleRelease
    - bundle exec fastlane internal
  artifacts:
    paths:
      - app/build/outputs/
      - riderz-release-key.keystore

pages:
  stage: deploy
  image: node:10
  before_script:
    - npm install gitbook-cli -g # install gitbook
    - gitbook fetch 3.2.3 # fetch final stable version
    - gitbook install # add any requested plugins in book.json
    - gitbook build . public # build to public path

  # add 'node_modules' to cache for speeding up builds
  cache:
    paths:
      - node_modules/ # Node modules and dependencies

  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
  only:
    - master
