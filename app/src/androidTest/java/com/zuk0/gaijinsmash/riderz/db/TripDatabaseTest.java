package com.zuk0.gaijinsmash.riderz.db;

import android.content.Context;

import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.zuk0.gaijinsmash.riderz.data.local.entity.trip.Trip;
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.TripDao;
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(AndroidJUnit4.class)
public class TripDatabaseTest {
    private TripDao mTripDao;
    private RiderzDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, RiderzDatabase.class).build();
        mTripDao = mDb.tripDao();
    }

    @After
    public void closeDb() throws IOException {
        // mDb.close();
    }

    @Test
    public void writeToDb() throws Exception {
        Trip trip = new Trip();
        trip.setId(1);
        trip.setOrigin("danville");
        mTripDao.save(trip);
    }

    @Test
    public void readFromDb() throws Exception {
        Trip trip = mTripDao.getTripById(1);
        assert (trip.getOrigin().equalsIgnoreCase("danville"));
    }
}
