package com.zuk0.gaijinsmash.riderz.db;

import android.content.Context;

import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite;
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.FavoriteDao;
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(AndroidJUnit4.class)
public class FavoriteDatabaseTest {
    private FavoriteDao mFavoriteDao;
    private RiderzDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, RiderzDatabase.class).build();
        mFavoriteDao = mDb.favoriteDao();
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void writeFavoriteToDatabase() throws Exception {
        Favorite favorite = new Favorite();
        favorite.setId(1);
        // favorite.setOrigin("danville");
        // favorite.setDestination("san ramon");
        mFavoriteDao.save(favorite);
    }

    @Test
    public void readFavoriteFromDatabase() throws IOException {
        Favorite favorite = mFavoriteDao.getFavoriteById(1);
        // assert(favorite.getOrigin().equals("danville"));
        // ßassert(favorite.getDestination().equals("san ramon"));
    }
}
