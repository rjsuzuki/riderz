package com.zuk0.gaijinsmash.riderz.db;

import android.content.Context;

import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station;
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.StationDao;
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(AndroidJUnit4.class)
public class StationDatabaseTest {
    private StationDao mStationDAO;
    private RiderzDatabase mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, RiderzDatabase.class).build();
        mStationDAO = mDb.stationDao();
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void writeStationToDatabase() throws Exception {
        Station station = new Station();
        station.setId(11);
        station.setName("danville");
        mStationDAO.save(station);
    }

    @Test
    public void readStationFromDatabase() throws IOException {
        Station station = mStationDAO.getStationByID(11);
        assert (station.getName().equalsIgnoreCase("danville"));
    }
}
