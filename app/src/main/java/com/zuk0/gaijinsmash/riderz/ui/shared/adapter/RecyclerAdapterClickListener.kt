package com.zuk0.gaijinsmash.riderz.ui.shared.adapter

import android.view.View

interface RecyclerAdapterClickListener {
    fun setClickListener(listener: View.OnClickListener?)
}
