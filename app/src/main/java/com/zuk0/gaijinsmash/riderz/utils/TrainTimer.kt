package com.zuk0.gaijinsmash.riderz.utils

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.orhanobut.logger.Logger

class TrainTimer : DefaultLifecycleObserver {

    private var startTimeStamp = 0L
    private var stopTimeStamp = 0L
    private var startingTimeInMillis = 10 * 60000L // default is 15 minutes, though i doubt anyone would leave the app open that long
    private var timer: CountDownTimer? = null
    var timeStampOfUpdateLiveData = MutableLiveData<Long>()

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        resume()
    }

    fun resume() {
        timer = getCountDownTimer(timeStampOfUpdateLiveData, startingTimeInMillis)
        timer?.start()
    }

    override fun onPause(owner: LifecycleOwner) {
        paused()
        super.onPause(owner)
    }

    fun paused() {
        stopTimeStamp = System.currentTimeMillis()
        timer?.cancel()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        destroy()
        super.onDestroy(owner)
    }

    fun destroy() {
        LogUtil.log(Logger.INFO, TAG, "destroy timer")
        timer?.cancel()
        timer = null
    }

    private fun getCountDownTimer(liveData: MutableLiveData<Long>, startingTimeInMillis: Long): CountDownTimer {
        return object : CountDownTimer(startingTimeInMillis, 60 * 1000) { // 60 second interval
            override fun onTick(millisUntilFinished: Long) {
                Log.i(TAG, "onTick: $millisUntilFinished")
                liveData.postValue(millisUntilFinished)
            }

            override fun onFinish() {
                Log.i(TAG, "onFinish")
                liveData.postValue(TIMER_FINISHED)
            }
        }
    }

    companion object {
        const val TAG = "TrainTimer"
        const val TIMER_FINISHED = -1L
    }
}
