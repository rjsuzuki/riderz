package com.zuk0.gaijinsmash.riderz.data.local.entity.etd

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.simpleframework.xml.Element

class Estimate {
    @SerializedName("origin")
    @Expose
    @field:Element(name = "origin", required = false)
    var origin: String? = null

    @SerializedName("destination")
    @Expose
    @field:Element(name = "destination", required = false)
    var destination: String? = null

    @SerializedName("minutes")
    @Expose
    @field:Element(name = "minutes", required = false)
    var minutes: String? = null

    @SerializedName("platform")
    @Expose
    @field:Element(name = "platform", required = false)
    var platform = 0

    @SerializedName("direction")
    @Expose
    @field:Element(name = "direction", required = false)
    var direction: String? = null

    @SerializedName("length")
    @Expose
    @field:Element(name = "length", required = false)
    var length = 0

    @SerializedName("color")
    @Expose
    @field:Element(name = "color", required = false)
    var color: String? = null

    @SerializedName("hexcolor")
    @Expose
    @field:Element(name = "hexcolor", required = false)
    var hexcolor: String? = null

    @SerializedName("bikeflag")
    @Expose
    @field:Element(name = "bikeflag", required = false)
    var bikeflag = 0

    @SerializedName("delay")
    @Expose
    @field:Element(name = "delay", required = false)
    var delay = 0
    var trainHeaderStation: String? = null
}
