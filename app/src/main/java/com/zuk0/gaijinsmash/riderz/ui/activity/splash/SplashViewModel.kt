package com.zuk0.gaijinsmash.riderz.ui.activity.splash

import android.app.Application
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.BuildConfig
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase
import com.zuk0.gaijinsmash.riderz.data.remote.repository.StationRepository
import com.zuk0.gaijinsmash.riderz.utils.LogUtil
import com.zuk0.gaijinsmash.riderz.utils.ThemeUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SplashViewModel
@Inject constructor(application: Application, val db: RiderzDatabase, val stationRepository: StationRepository) : AndroidViewModel(application) {

    fun migrateDb() {
        if (BuildConfig.VERSION_CODE <= 30200) {
            Logger.i("Updating sqlite database")
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    db.clearAllTables()
                }
                initStationsData()
            }
        }
    }

    fun initStationsData() {
        Logger.i("Loading stations into db...")
        viewModelScope.launch(Dispatchers.IO) {
            val count = db.stationDao().countStations()
            Logger.d("local stations count: $count")

            if (count < BART_STATIONS_COUNT) {
                stationRepository.loadStations(getApplication())
            } else {
                stationRepository.refreshStation("BERY")
                stationRepository.refreshStation("MLPT")
            }
        }
    }

    fun initDayNightTheme() {
        // if no preference is saved or Logged Out, app will default to SYSTEM
        val currentMode = AppCompatDelegate.getDefaultNightMode()
        var savedMode = ThemeUtil.getSavedThemePreference(getApplication())
        if (currentMode != savedMode) {
            LogUtil.log("Theme mode initialized: $savedMode")
            if (savedMode == AppCompatDelegate.MODE_NIGHT_UNSPECIFIED) {
                savedMode = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
                    AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
                } else {
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                }
            }
            AppCompatDelegate.setDefaultNightMode(savedMode)
        }
    }

    companion object {

        /*
        TODO: update this count whenever a new station is built
        grab entire list first time and save count to shared prefs.
        check shared prefs for cou
     */
        private const val BART_STATIONS_COUNT = 49 // berryessa is the latest
    }
}
