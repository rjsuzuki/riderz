package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.bsa

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.zuk0.gaijinsmash.riderz.BuildConfig
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.Bsa
import com.zuk0.gaijinsmash.riderz.data.local.manager.AppReviewManager
import com.zuk0.gaijinsmash.riderz.data.local.model.ads.BannerAd
import com.zuk0.gaijinsmash.riderz.data.local.model.ads.RiderzAd
import com.zuk0.gaijinsmash.riderz.databinding.AdBannerBinding
import com.zuk0.gaijinsmash.riderz.databinding.ListRowBsaBinding
import com.zuk0.gaijinsmash.riderz.databinding.UserRatingLayoutBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.HomeViewModel

class BsaRecyclerAdapter(private val advisoryList: MutableList<Bsa>, private val vm: HomeViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isAdViewEnabled = false
    private var isUserRatingVisible = false
    private var recyclerView: RecyclerView? = null
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> {
                when {
                    isUserRatingVisible -> USER_RATING_TYPE
                    isAdViewEnabled -> AD_TYPE
                    else -> BSA_TYPE
                }
            }

            else -> super.getItemViewType(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            AD_TYPE -> {
                val binding = AdBannerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                AdViewHolder(binding)
            }

            USER_RATING_TYPE -> {
                val binding = UserRatingLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                UserRatingViewHolder(binding)
            }

            else -> {
                val binding = DataBindingUtil.inflate<ListRowBsaBinding>(LayoutInflater.from(parent.context), R.layout.list_row_bsa, parent, false)
                BsaViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BsaViewHolder -> {
                val bsa = advisoryList[position]
                holder.advisoryBinding.bsa = bsa
                if (bsa.station == null) {
                    holder.advisoryBinding.bsaStationTextView.visibility = View.GONE
                }
                if (bsa.type == null) {
                    holder.advisoryBinding.bsaTypeTextView.visibility = View.GONE
                } else if (bsa.type.equals(DELAY, ignoreCase = true)) {
                    holder.advisoryBinding.bsaTypeTextView.visibility = View.VISIBLE
                    val errorDrawable = ResourcesCompat.getDrawable(holder.advisoryBinding.root.resources, R.drawable.ic_error_outline_black_24dp, holder.advisoryBinding.root.context.theme)
                    holder.advisoryBinding.bsaStatusImageView.setImageDrawable(errorDrawable)
                    holder.advisoryBinding.bsaStatusImageView.imageTintList = ResourcesCompat.getColorStateList(holder.advisoryBinding.root.resources, R.color.colorSecondary, holder.advisoryBinding.root.context.theme)
                }
                // Reveal animation
                holder.advisoryBinding.container.animation = AnimationUtils.loadAnimation(holder.advisoryBinding.root.context, R.anim.slide_in_right)

                // Hide animation
                holder.advisoryBinding.bsaDismissBtn.setOnClickListener {
                    dismissCard(holder.advisoryBinding.container, position)
                }
            }

            is UserRatingViewHolder -> {
                // Reveal animation
                holder.userRatingLayoutBinding.root.animation = AnimationUtils.loadAnimation(holder.userRatingLayoutBinding.root.context, R.anim.slide_in_right)
                when (vm.appReviewManager.getState()) {
                    AppReviewManager.ReviewState.UNCONFIRMED -> {
                        holder.userRatingLayoutBinding.title.text = holder.userRatingLayoutBinding.root.context.getString(R.string.user_rating_title2)
                        holder.userRatingLayoutBinding.sendBtn.text = holder.userRatingLayoutBinding.root.context.getString(R.string.user_rating_forgot)
                        holder.userRatingLayoutBinding.laterBtn.text = holder.userRatingLayoutBinding.root.context.getString(R.string.user_rating_already_done)
                    }

                    else -> Log.d(TAG, "New app review request.")
                }

                holder.userRatingLayoutBinding.laterBtn.setOnClickListener {
                    Log.d(TAG, "Delayed Rating: ${holder.userRatingLayoutBinding.ratingBar.rating}")
                    vm.appReviewManager.updateUserRatingAction(vm.getApplication(), AppReviewManager.UserAction.NO)
                    dismissCard(holder.userRatingLayoutBinding.root, position)
                }
                holder.userRatingLayoutBinding.sendBtn.setOnClickListener {
                    Log.d(TAG, "Rating selected: ${holder.userRatingLayoutBinding.ratingBar.rating}")
                    vm.appReviewManager.updateUserRatingAction(vm.getApplication(), AppReviewManager.UserAction.YES)
                    dismissCard(holder.userRatingLayoutBinding.root, position)
                    vm.appReviewManager.launchPlayStoreIntent(vm.getApplication())
                }
            }

            is AdViewHolder -> {
                holder.initAds()
            }
        }
    }

    private fun dismissCard(card: View, position: Int) {
        val listener = object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation?) {
                // Remove the current card
                card.visibility = View.GONE
                advisoryList.removeAt(position)
                notifyItemRemoved(position)

                // Check if new card is needed
                if (advisoryList.size == 0) {
                    if (vm.appReviewManager.isReviewWanted(card.context)) {
                        isUserRatingVisible = true
                        advisoryList.add(0, Bsa())
                        notifyItemChanged(0)
                    } else {
                        isUserRatingVisible = false
                        isAdViewEnabled = true
                        advisoryList.add(0, Bsa())
                        notifyItemChanged(0)
                    }
                }
            }

            override fun onAnimationRepeat(animation: Animation?) {
                // do nothing
            }

            override fun onAnimationStart(animation: Animation?) {
                // do nothing
            }
        }
        val exitAnimation = AnimationUtils.loadAnimation(card.context, R.anim.slide_out_left)
        exitAnimation.setAnimationListener(listener)
        card.startAnimation(exitAnimation)
    }

    override fun getItemCount(): Int {
        return advisoryList.size
    }

    fun update(newData: List<Bsa>) {
        val diffResult = DiffUtil.calculateDiff(BsaDiffCallback(advisoryList, newData))
        advisoryList.clear()
        advisoryList.addAll(newData)
        diffResult.dispatchUpdatesTo(this)
    }

    internal inner class BsaDiffCallback(private val oldList: List<Bsa>, private val newList: List<Bsa>) : DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }

    class BsaViewHolder(val advisoryBinding: ListRowBsaBinding) : RecyclerView.ViewHolder(advisoryBinding.root)
    class UserRatingViewHolder(val userRatingLayoutBinding: UserRatingLayoutBinding) : RecyclerView.ViewHolder(userRatingLayoutBinding.root.rootView)

    class AdViewHolder(private val adBinding: AdBannerBinding) : RecyclerView.ViewHolder(adBinding.root) {
        private var ad: BannerAd? = null

        init {
            Log.v("AdViewHolder", "init")
        }

        fun initAds() {
            if (ad != null) {
                ad = null
            }
            ad = BannerAd("HomeBanner", if (BuildConfig.DEBUG) RiderzAd.TEST_BANNER_ID else RiderzAd.HOME_BANNER_ID)
            val adView = AdView(adBinding.root.context)
            adView.setAdSize(AdSize.LARGE_BANNER)
            ad?.setAdView(adView)
            adBinding.adView.addView(adView)
        }
    }

    companion object {
        private const val TAG = "BsaRecyclerAdapter"
        private const val DELAY = "DELAY"
        private const val AD_TYPE = 333
        private const val USER_RATING_TYPE = 222
        private const val BSA_TYPE = 111
    }
}
