package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.favorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite
import com.zuk0.gaijinsmash.riderz.data.local.entity.diffutil.FavoriteDiffCallback
import com.zuk0.gaijinsmash.riderz.databinding.ListRowFavorites2Binding
import com.zuk0.gaijinsmash.riderz.ui.shared.adapter.RecyclerAdapterClickListener

class FavoriteRecyclerAdapter(var favoriteList: MutableList<Favorite>, private val vm: FavoritesViewModel) : RecyclerView.Adapter<FavoriteViewHolder>(), RecyclerAdapterClickListener {

    private var listener: View.OnClickListener? = null // todo  - use this as general listener for all buttons

    override fun setClickListener(listener: View.OnClickListener?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val binding = ListRowFavorites2Binding.inflate(LayoutInflater.from(parent.context))
        return FavoriteViewHolder(binding, this, vm)
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val favorite = favoriteList[position]
        holder.bind(favorite)
    }

    override fun getItemCount(): Int {
        return favoriteList.size
    }

    /**
     * Public Method
     * Diff util - updates the recyclerview asynchronously
     */
    fun updateList(newList: MutableList<Favorite>?) {
        val oldList = mutableListOf<Favorite>()
        oldList.addAll(favoriteList)
        favoriteList.clear()
        newList?.let {
            favoriteList.addAll(newList)
        }
        val diffResult = DiffUtil.calculateDiff(FavoriteDiffCallback(oldList, favoriteList))
        diffResult.dispatchUpdatesTo(this)

        if (itemCount == 0) {
            vm.isFavoriteCardViewLiveData.postValue(true)
        }
    }
}
