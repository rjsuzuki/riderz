package com.zuk0.gaijinsmash.riderz.data.local.room.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdXmlResponse

@Dao
interface EtdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(etd: EtdXmlResponse?)

    @Query("SELECT * FROM estimates where :originAbbr = originAbbr")
    fun getLatest(originAbbr: String): EtdXmlResponse?

    @Delete
    fun delete(etd: EtdXmlResponse)

    @Query("SELECT * FROM estimates where :originAbbr = originAbbr")
    fun getAll(originAbbr: String): List<EtdXmlResponse>?
}
