package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.trip

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.BuildConfig
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.model.ads.BannerAd
import com.zuk0.gaijinsmash.riderz.data.local.model.ads.RiderzAd
import com.zuk0.gaijinsmash.riderz.databinding.FragmentTripBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment
import com.zuk0.gaijinsmash.riderz.utils.KeyboardUtils
import com.zuk0.gaijinsmash.riderz.utils.SharedPreferencesUtils
import com.zuk0.gaijinsmash.riderz.utils.TimeDateUtils
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.Objects

class TripFragment : BaseFragment() {

    private lateinit var binding: FragmentTripBinding
    private lateinit var vm: TripViewModel
    private var timePickerDialog: TimePickerDialog? = null
    private var datePickerDialog: DatePickerDialog? = null
    private var simpleDateFormat: SimpleDateFormat? = null
    private var is24HrTimeOn = false
    private var spinnerAdapter: ArrayAdapter<String>? = null
    private var ad: BannerAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentTripBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.collapseAppBar(activity)
        super.setTitle(activity, getString(R.string.trip_title))
        initTimePreference(activity)
        initStationsList()
        initSpinnerAdapter()
        initKeyboardSettings()
        initDatePicker()
        initTimePicker()
        initTextInputEditors()
        initSearchButton()
        initDepartingSpinner(spinnerAdapter)
        initArrivingSpinner(spinnerAdapter)
        initAds()
    }

    private fun initAds() {
        ad = BannerAd("TripBanner", if (BuildConfig.DEBUG) RiderzAd.TEST_BANNER_ID else RiderzAd.TRIP_BANNER_ID)
        val adView = AdView(requireContext())
        adView.setAdSize(AdSize.LARGE_BANNER)
        ad?.setAdView(adView)
        binding.tripAdBanner.adView.addView(adView)
    }

    private fun initViewModel() {
        vm = ViewModelProvider(this)[TripViewModel::class.java]
    }

    private fun initTimePreference(context: Context?) {
        is24HrTimeOn = SharedPreferencesUtils.getTimePreference(context)
    }

    private fun initStationsList() {
        // This data is to provide a station list for Spinner and AutoCompleteTextView
        vm.stationsList = resources.getStringArray(R.array.stations_list)
    }

    private fun initSpinnerAdapter() {
        spinnerAdapter = ArrayAdapter(requireContext(), R.layout.custom_dropdown_item, vm.stationsList)
        spinnerAdapter?.setDropDownViewResource(R.layout.custom_dropdown_item)
    }

    private fun initKeyboardSettings() {
        binding.tripDepartureAutoCompleteTextView.setImeActionLabel(resources.getString(R.string.button_next), KeyEvent.KEYCODE_ENTER)
        binding.tripDepartureAutoCompleteTextView.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    binding.tripArrivalAutoCompleteTextView.requestFocus()
                    KeyboardUtils.openKeyboard(context)
                    return true
                }
                return false
            }
        })
        binding.tripArrivalAutoCompleteTextView.setImeActionLabel(resources.getString(R.string.button_next), KeyEvent.KEYCODE_ENTER)
        binding.tripArrivalAutoCompleteTextView.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    binding.tripDateEditText.requestFocus()
                    KeyboardUtils.closeKeyboard(activity)
                    return true
                }
                return false
            }
        })
        binding.tripDateEditText.setImeActionLabel(resources.getString(R.string.button_next), KeyEvent.KEYCODE_ENTER)
        binding.tripDateEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    binding.tripTimeEditText.requestFocus()
                    return true
                }
                return false
            }
        })
        binding.tripTimeEditText.setImeActionLabel(resources.getString(R.string.button_search), KeyEvent.KEYCODE_ENTER)
        binding.tripTimeEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    getUserInputFields()
                    attemptTripSearch(vm.originField.trim(), vm.destinationField.trim(), vm.dateField, vm.timeField)
                    return true
                }
                return false
            }
        })
    }

    private fun initDepartingSpinner(adapter: ArrayAdapter<String>?) {
        binding.tripDepartureAutoCompleteTextView.setDropDownBackgroundResource(R.color.primaryDarkColor)
        binding.stationSpinner1.adapter = adapter
        binding.stationSpinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Logger.i("position $position")
                binding.stationSpinner1.selectedView?.let {
                    val textView = binding.stationSpinner1.selectedView as TextView
                    var itemSelected = ""
                    itemSelected = textView.text.toString()
                    binding.tripDepartureAutoCompleteTextView.setText(itemSelected)
                    KeyboardUtils.closeKeyboard(activity)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
                Logger.e("todo")
            }
        }
    }

    private fun initArrivingSpinner(adapter: ArrayAdapter<String>?) {
        binding.tripArrivalAutoCompleteTextView.setDropDownBackgroundResource(R.color.primaryDarkColor)
        binding.stationSpinner2.adapter = adapter
        binding.stationSpinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                binding.stationSpinner2.selectedView?.let {
                    val textView = binding.stationSpinner2.selectedView as TextView
                    var itemSelected = ""
                    itemSelected = textView.text?.toString() ?: ""
                    binding.tripArrivalAutoCompleteTextView.setText(itemSelected)
                    KeyboardUtils.closeKeyboard(activity)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
                Logger.e("todo")
            }
        }
    }

    private fun initDatePicker() {
        binding.tripDateEditText.setText(getText(R.string.currentDate))
        binding.tripDateEditText.inputType = InputType.TYPE_NULL
        binding.tripDateEditText.requestFocus()
        binding.tripDateEditText.setOnClickListener { _ ->
            val newCalendar = Calendar.getInstance()
            context?.let {
                datePickerDialog = DatePickerDialog(
                    it,
                    { _, year, month, day ->
                        val newDate = Calendar.getInstance()
                        newDate.set(year, month, day)
                        simpleDateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.US)
                        binding.tripDateEditText.setText(simpleDateFormat!!.format(newDate.time))
                    },
                    newCalendar.get(Calendar.YEAR),
                    newCalendar.get(Calendar.MONTH),
                    newCalendar.get(Calendar.DAY_OF_MONTH),
                )

                datePickerDialog?.show()
                datePickerDialog?.getButton(DatePickerDialog.BUTTON_POSITIVE)?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorSecondary, context?.theme))
                datePickerDialog?.getButton(DatePickerDialog.BUTTON_NEGATIVE)?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorSecondary, context?.theme))
            }
        }
    }

    private fun initTimePicker() {
        binding.tripTimeEditText.setText(getText(R.string.currentTime))
        binding.tripTimeEditText.inputType = InputType.TYPE_NULL
        binding.tripTimeEditText.requestFocus()
        binding.tripTimeEditText.setOnClickListener {
            // Current time is default value
            val currentTime = Calendar.getInstance()
            val hour = currentTime.get(Calendar.HOUR_OF_DAY)
            val minute = currentTime.get(Calendar.MINUTE)

            // Create and return a new instance of TimePickerDialog
            timePickerDialog = TimePickerDialog(
                activity,
                { _, selectedHour, selectedMinute ->
                    // Returned value is always 24hr so conversion is necessary
                    if (is24HrTimeOn) {
                        val formattedTime = String.format(Locale.US, "%02d:%02d", selectedHour, selectedMinute)
                        binding.tripTimeEditText.setText(formattedTime)
                    } else {
                        val convertedTime = TimeDateUtils.convertTo12HrForTrip("$selectedHour:$selectedMinute")
                        binding.tripTimeEditText.setText(convertedTime)
                    }
                },
                hour,
                minute,
                is24HrTimeOn,
            ) // True = 24 hour format on TimePicker only
            timePickerDialog?.setTitle(getString(R.string.time_title))
            timePickerDialog?.show()
            timePickerDialog?.getButton(TimePickerDialog.BUTTON_POSITIVE)?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorSecondary, context?.theme))
            timePickerDialog?.getButton(TimePickerDialog.BUTTON_NEGATIVE)?.setTextColor(ResourcesCompat.getColor(resources, R.color.colorSecondary, context?.theme))
        }
    }

    private fun initTextInputEditors() {
        val textViewAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_selectable_list_item, vm.stationsList)
        binding.tripDepartureAutoCompleteTextView.threshold = 1 // will start working from first character
        binding.tripDepartureAutoCompleteTextView.setAdapter(textViewAdapter)
        binding.tripDepartureAutoCompleteTextView.requestFocus()

        binding.tripArrivalAutoCompleteTextView.threshold = 1
        binding.tripArrivalAutoCompleteTextView.setAdapter(textViewAdapter)
        binding.tripArrivalAutoCompleteTextView.setOnDismissListener {
            KeyboardUtils.closeKeyboard(activity)
        }
    }

    private fun initSearchButton() {
        binding.tripButton.setOnClickListener {
            getUserInputFields()
            attemptTripSearch(vm.originField.trim(), vm.destinationField.trim(), vm.dateField, vm.timeField)
        }
    }

    private fun getUserInputFields() {
        /*
            NOTE: stations have not been formatted to their abbreviated versions yet.
            From the BartResultsFragment, take String values from Bundle
            and convert them appropriately before making API call
        */
        vm.originField = binding.tripDepartureAutoCompleteTextView.text.toString()
        vm.destinationField = binding.tripArrivalAutoCompleteTextView.text.toString()

        // DATE
        vm.dateField = Objects.requireNonNull<Editable>(binding.tripDateEditText.text).toString()

        // TIME
        val preformatTime = Objects.requireNonNull<Editable>(binding.tripTimeEditText.text).toString()
        vm.timeField = vm.getTimeForTripSearch(preformatTime, is24HrTimeOn)
    }

    private fun validateUserInput(departingStation: String, arrivingStation: String, departingDate: String, departingTime: String): Boolean {
        // check if all fields are filled
        if (departingStation.isBlank()) {
            binding.tripDepartureAutoCompleteTextView.error = getString(R.string.error_form_completion)
            return false
        }
        if (arrivingStation.isEmpty()) {
            binding.tripArrivalAutoCompleteTextView.error = getString(R.string.error_form_completion)
            return false
        }
        if (departingDate.isEmpty()) {
            binding.tripDateEditText.error = getString(R.string.error_form_completion)
            return false
        }
        if (departingTime.isEmpty()) {
            binding.tripTimeEditText.error = getString(R.string.error_form_completion)
            return false
        }

        // check if origin and destination are different
        if (departingStation == arrivingStation) {
            binding.tripArrivalAutoCompleteTextView.error = getString(R.string.error_form_completion2)
            return false
        }

        // check if both stations are matching in strings resource
        if (!vm.doesStationExist(departingStation, context)) {
            binding.tripDepartureAutoCompleteTextView.error = getString(R.string.error_station_not_found)
            return false
        }

        if (!vm.doesStationExist(arrivingStation, context)) {
            binding.tripArrivalAutoCompleteTextView.error = getString(R.string.error_station_not_found)
            return false
        }
        return true
    }

    private fun attemptTripSearch(origin: String, destination: String, date: String, time: String) {
        if (validateUserInput(origin, destination, date, time)) {
            launchTripResultFragment(origin, destination, date, time)
        } else {
            Logger.w("User input invalid")
        }
    }

    private fun launchTripResultFragment(origin: String, destination: String, date: String, time: String) {
        val bundle = initBundler(origin, destination, date, time)
        NavHostFragment.findNavController(this).navigate(R.id.action_tripFragment_to_resultsFragment, bundle, null, null)
    }

    private fun initBundler(origin: String, destination: String, date: String, time: String): Bundle { // TODO
        val bundle = Bundle()
        bundle.putString(TripBundle.ORIGIN.value, origin)
        bundle.putString(TripBundle.DESTINATION.value, destination)
        bundle.putString(TripBundle.DATE.value, date)
        bundle.putString(TripBundle.TIME.value, time)
        return bundle
    }

    enum class TripBundle(val value: String) { // TODO
        ORIGIN("Origin"), DESTINATION("Destination"), DATE("DATE"), TIME("TIME"), TRAIN_HEADERS("TrainHeaders")
    }
}
