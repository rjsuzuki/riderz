package com.zuk0.gaijinsmash.riderz.data.local.entity.trip

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station

class Root {
    @SerializedName("@id")
    @Expose
    var id: String? = null

    @SerializedName("uri")
    @Expose
    var uri: Uri? = null

    @SerializedName("origin")
    @Expose
    var origin: String? = null

    @SerializedName("destination")
    @Expose
    var destination: String? = null

    @SerializedName("sched_num")
    @Expose
    var schedNum: String? = null

    @SerializedName("schedule")
    @Expose
    var schedule: Schedule? = null

    @SerializedName("message")
    @Expose
    var message = ""

    @SerializedName("station")
    @Expose
    var station: List<Station>? = null
}
