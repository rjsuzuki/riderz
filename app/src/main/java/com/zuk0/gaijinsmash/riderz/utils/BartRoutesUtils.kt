package com.zuk0.gaijinsmash.riderz.utils

import android.content.Context
import android.content.res.ColorStateList
import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.zuk0.gaijinsmash.riderz.R

object BartRoutesUtils {
    fun setLineBarByRoute(context: Context, route: String?, coloredBar: TextView) {
        when (route) {
            "ROUTE 1" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartYellowLine, context.theme))
            "ROUTE 2" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartYellowLine, context.theme))
            "ROUTE 3" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartOrangeLine, context.theme))
            "ROUTE 4" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartOrangeLine, context.theme))
            "ROUTE 5" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartGreenLine, context.theme))
            "ROUTE 6" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartGreenLine, context.theme))
            "ROUTE 7" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartRedLine, context.theme))
            "ROUTE 8" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartRedLine, context.theme))
            "ROUTE 9" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartBlueLine, context.theme))
            "ROUTE 10" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartBlueLine, context.theme))
            "ROUTE 11" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartBlueLine, context.theme))
            "ROUTE 12" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartBlueLine, context.theme))
            "ROUTE 13" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartPurpleLine, context.theme))
            "ROUTE 14" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartPurpleLine, context.theme))
            "ROUTE 19" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartOakAirport, context.theme))
            "ROUTE 20" -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartOakAirport, context.theme))
            else -> coloredBar.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartDefault, context.theme))
        }
    }

    fun setLineBarByColor(context: Context, color: String?, line: View) {
        val colorStateList: ColorStateList?
        when (color) {
            "YELLOW" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartYellowLine, context.theme)
                // line.backgroundTintList(ResourcesCompat.getColor(context.resources, R.color.bartYellowLine))
            }
            "GREEN" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartGreenLine, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartGreenLine))
            }
            "RED" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartRedLine, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartRedLine))
            }
            "ORANGE" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartOrangeLine, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartOrangeLine))
            }
            "BLUE" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartBlueLine, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartBlueLine))
            }
            "PURPLE" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartPurpleLine, context.theme)

                //  line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartPurpleLine))
            }
            "GRAY" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartOakAirport, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartOakAirport))
            }
            "WHITE" -> {
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartDefault, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartDefault))
            }
            else -> {
                LogUtil.log("Unknown train line : $color")
                colorStateList = ResourcesCompat.getColorStateList(context.resources, R.color.bartGreenLine, context.theme)

                // line.setBackgroundColor(ResourcesCompat.getColor(context.resources, R.color.bartDefault))
            }
        }
        line.backgroundTintList = colorStateList
    }
}
