package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stations

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.StationXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase
import com.zuk0.gaijinsmash.riderz.data.remote.repository.StationRepository
import javax.inject.Inject

class StationsViewModel
@Inject constructor(val mRepository: StationRepository, val db: RiderzDatabase) : ViewModel() {

    private var mListFromRepo: LiveData<StationXmlResponse>? = null

    fun getListFromDb(context: Context?): LiveData<List<Station>>? {
        return db.stationDao().stationsLiveData
    }

    val listFromRepo: LiveData<StationXmlResponse>
        get() {
            if (mListFromRepo == null) {
                mListFromRepo = mRepository.stations
            }
            return mListFromRepo!!
        }
}
