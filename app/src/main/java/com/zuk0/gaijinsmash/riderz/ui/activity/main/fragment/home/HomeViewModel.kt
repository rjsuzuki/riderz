package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home

import android.app.Application
import android.content.Context
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite
import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.BsaJsonResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.BsaXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.Estimate
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.Etd
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station
import com.zuk0.gaijinsmash.riderz.data.local.entity.trip.Trip
import com.zuk0.gaijinsmash.riderz.data.local.entity.trip.TripJsonResponse
import com.zuk0.gaijinsmash.riderz.data.local.manager.AppReviewManager
import com.zuk0.gaijinsmash.riderz.data.local.manager.LocationManager
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase
import com.zuk0.gaijinsmash.riderz.data.remote.repository.BsaRepository
import com.zuk0.gaijinsmash.riderz.data.remote.repository.EtdRepository
import com.zuk0.gaijinsmash.riderz.data.remote.repository.TripRepository
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.bsa.BsaRecyclerAdapter
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.etd.PlatformRecyclerAdapter
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper
import com.zuk0.gaijinsmash.riderz.utils.HaversineFormulaUtils
import com.zuk0.gaijinsmash.riderz.utils.SharedPreferencesUtils
import com.zuk0.gaijinsmash.riderz.utils.StationUtils
import com.zuk0.gaijinsmash.riderz.utils.TimeDateUtils
import com.zuk0.gaijinsmash.riderz.utils.TrainTimer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.sql.Timestamp
import java.util.Date
import javax.inject.Inject
import kotlin.collections.HashMap
import kotlin.collections.set

class HomeViewModel @Inject
constructor(
    application: Application,
    private val tripRepository: TripRepository,
    private val bsaRepository: BsaRepository,
    private val etdRepository: EtdRepository,
    private val db: RiderzDatabase,
) : AndroidViewModel(application) {

    var refreshCount = 0
    var refreshMax = 2 // 2 taps within 5 minutes

    val appReviewManager = AppReviewManager

    var savedInstanceState: Bundle? = null
    var closestStation: Station? = null
    var selectedStation: Station? = null
    private var userLocation: Location? = null
    val timer = TrainTimer()

    var station: Station? = null

    val isEtdRefreshNeeded = MutableLiveData<Boolean>()

    // adapters
    var bsaAdapter: BsaRecyclerAdapter? = null
    var platformRecyclerAdapter: PlatformRecyclerAdapter? = null

    fun getLocation(): Location? {
        if (userLocation == null) {
            val gps = LocationManager(getApplication())
            return gps.location
        }
        return userLocation
    }

    // Communication Bridges
    val isLocationPermissionEnabledLD = MutableLiveData<Boolean>()
    private val closestStationLiveData = MutableLiveData<Station>()
    val navigationLiveData = MutableLiveData<String>()

    val platform1 = mutableListOf<Etd>()
    val platform2 = mutableListOf<Etd>()

    val bsaLiveData: LiveData<BsaXmlResponse>
        get() = bsaRepository.getBsa()

    fun getBsaJson(): LiveData<BsaJsonResponse> {
        return bsaRepository.getBsaJson(getApplication())
    }

    fun is24HrTimeOn(context: Context): Boolean {
        return SharedPreferencesUtils.getTimePreference(context)
    }

    private fun initTime(is24HrTimeOn: Boolean, time: String): String {
        val result: String
        if (is24HrTimeOn) {
            result = TimeDateUtils.format24hrTime(time)
        } else {
            result = TimeDateUtils.convertTo12Hr(time)
        }
        return result
    }

    fun getEtdLiveData(origin: String): LiveData<LiveDataWrapper<EtdXmlResponse>> {
        val originAbbr = StationUtils.getAbbrFromStationName(origin)
        return etdRepository.getEtd(originAbbr)
    }

    fun setTrainHeaders(trips: List<Trip>, favorite: Favorite) {
        val trainHeaders = ArrayList<String>()
        for (trip in trips) {
            val header = StationUtils.getAbbrFromStationName(trip.legList?.get(0)?.trainHeadStation)?.toUpperCase()
            if (!trainHeaders.contains(header) && header != null) {
                trainHeaders.add(header)
                Log.i("HEADER", header)
            }
        }
        favorite.trainHeaderStations = trainHeaders // todo: use hashset
    }

    fun createFavoriteInverse(trips: List<Trip>, favorite: Favorite): Favorite {
        val favoriteInverse = Favorite()
        favoriteInverse.a = favorite.b
        favoriteInverse.b = favorite.a
        setTrainHeaders(trips, favoriteInverse)
        return favoriteInverse
    }

    internal fun getTripLiveData(origin: String, destination: String): LiveData<LiveDataWrapper<TripJsonResponse>> {
        return tripRepository.getTrip(
            StationUtils.getAbbrFromStationName(origin),
            StationUtils.getAbbrFromStationName(destination),
            "TODAY",
            "NOW",
        )
    }

    /*
    map is useful when you want to make changes to the value before dispatching it to the UI.
    switchMap is useful when you want to return different LiveData based upon the value of the first one.
     */

    // get station, then post value of
    fun getEstimatesLiveData(station: Station): LiveData<LiveDataWrapper<EtdXmlResponse>> {
        return etdRepository.getEtd(station.abbr)
    }

    /*
        For comparisons - make sure all train headers are abbreviated and capitalized
        For Setting estimates- use the full name of station
     */
    internal fun getEstimatesFromEtd(favorite: Favorite, etds: List<Etd>): MutableList<Estimate> {
        val results = ArrayList<Estimate>()
        for (etd in etds) {
            if (favorite.trainHeaderStations?.contains(etd.destinationAbbr?.toUpperCase()) == true) {
                val estimate = etd.estimateList?.get(0)
                estimate?.let {
                    estimate.origin = favorite.a?.name
                    estimate.destination = favorite.b?.name
                    estimate.trainHeaderStation = etd.destination
                    results.add(estimate)
                }
            }
        }
        return results
    }

    /**
     * get a list of Estimate objects
     */
    fun getEstimatesFromEtd(station: Station): MutableList<Estimate> {
        val origin = station.name
        val etds = station.etdList
        val results = ArrayList<Estimate>()
        etds?.let {
            for (etd in etds) {
                val estimate = etd.estimateList?.get(0)
                estimate?.let {
                    estimate.origin = origin
                    estimate.destination = etd.destination
                    results.add(estimate)
                }
            }
        }
        return results
    }

    internal fun checkHolidaySchedule() {
        // TODO: push news to home fragment if it's a holiday
        // https://www.bart.gov/guide/holidays
        // xmas, nye,
    }

    private val calculateDistances = FloatArray(2)

    /**
     * important!
     */
    private fun calculateDistanceBetween(endLat: Double, endLong: Double, startLat: Double, startLong: Double) {
        Location.distanceBetween(endLat, endLong, startLat, startLong, calculateDistances)
    }

    // get user location, use haversine formula to get nearest station.
    fun getNearestStation(userLocation: Location?): LiveData<Station> {
        if (userLocation == null) {
            return closestStationLiveData
        }

        viewModelScope.launch(Dispatchers.IO) {
            var closestDistance = 0
            val list = getStationsFromDb()
            for (station in list) {
                val stationLat = station.latitude
                val stationLong = station.longitude

                val distanceBetween = HaversineFormulaUtils.calculateDistanceInKilometer(
                    userLocation.latitude,
                    userLocation.longitude,
                    stationLat,
                    stationLong,
                )

                if (closestDistance == 0) {
                    closestDistance = distanceBetween
                    closestStation = station
                } else if (closestDistance > distanceBetween) {
                    closestDistance = distanceBetween
                    closestStation = station
                }
            }
            Logger.i("closest station: ${closestStation?.name}")
            closestStationLiveData.postValue(closestStation)
        }
        return closestStationLiveData
    }

    fun getStationsFromDb(): List<Station> {
        return db.stationDao().allStations
    }

    fun getFavoritesList(favorite: Favorite) {
        // todo update
    }

    // for each destination has its own destination
    fun createEstimateListsByPlatform(list: MutableList<Etd>?) {
        list?.let {
            val platformList = mutableListOf<MutableList<Etd>?>()

            for (i in list) {
                if (i.estimateList?.get(0)?.platform == 1) {
                    platform1.add(i)
                } else {
                    platform2.add(i)
                }
            }
        }
    }

    fun createEstimateListsByPlatform2(list: MutableList<Etd>?): HashMap<Int, MutableList<Etd>> {
        val estimateHashMap = HashMap<Int, MutableList<Etd>>()
        list?.let { etdList ->
            etdList.forEach { etd ->
                val platform = etd.estimateList?.get(0)?.platform
                if (platform != null) {
                    val currentList = estimateHashMap[platform]
                    if (currentList.isNullOrEmpty()) {
                        val newList = mutableListOf<Etd>()
                        newList.add(etd)
                        estimateHashMap[platform] = newList
                    } else {
                        currentList.add(etd)
                        estimateHashMap[platform] = currentList
                    }
                }
            }
        }
        return estimateHashMap
    }

    fun sortMapIntoList(map: HashMap<Int, MutableList<Etd>>): MutableList<MutableList<Etd>?> {
        val list = mutableListOf<MutableList<Etd>?>()
        for (i in 0 until map.size) {
            if (map.containsKey(i + 1)) {
                list.add(map.get(i + 1))
            }
        }
        return list
    }

    fun getPlatformTitle(list: List<Etd>): String {
        if (list.isNotEmpty()) {
            return list[0].estimateList?.get(0)?.platform.toString()
        }
        return ""
    }

    fun saveState(outState: Bundle) {
        // timer?.saveState(outState)
    }

    fun restoreState(savedInstanceState: Bundle) {
        this.savedInstanceState = savedInstanceState
        // timer?.create(savedInstanceState)
    }

    /**
     * Remove at position (Int) for EstimateRecyclerAdapter viewholders
     */
    val removeEstimateLiveData = MutableLiveData<Int>()

    var cachedTimeDifference = 0

    fun getMinutesPassedFromCache(cachedDate: Timestamp): Int {
        return TimeDateUtils.durationOfMinutesBetweenDates(cachedDate, Date(System.currentTimeMillis()))
    }

    override fun onCleared() {
        Log.i(TAG, "onCleared()")

        bsaAdapter = null
        platformRecyclerAdapter = null
        super.onCleared()
    }

    companion object {
        private val TAG = "HomeViewModel"
    }
}
