package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zuk0.gaijinsmash.riderz.BuildConfig
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.databinding.FragmentAboutBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment

class AboutFragment : BaseFragment() {

    private lateinit var binding: FragmentAboutBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentAboutBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.collapseAppBar(activity)
        super.setTitle(activity, getString(R.string.about_title))
        val versionTv = view.findViewById<TextView>(R.id.version_tv)
        versionTv.text = BuildConfig.VERSION_NAME

        binding.aboutPolicyBtn.setOnClickListener {
            Log.i(TAG, "Launching policy view")
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(POLICY_URL)
            startActivity(intent)
        }
    }

    companion object {
        private const val TAG = "AboutFragment"
        private const val POLICY_URL = "https://riderz.zuko.gg"
    }
}
