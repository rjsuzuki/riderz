package com.zuk0.gaijinsmash.riderz.data.remote.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.StationXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.StationDao
import com.zuk0.gaijinsmash.riderz.data.remote.retrofit.BartService
import com.zuk0.gaijinsmash.riderz.utils.NetworkUtils
import com.zuk0.gaijinsmash.riderz.utils.xmlparser.StationXmlParser
import org.xmlpull.v1.XmlPullParserException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.Executor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StationRepository
@Inject constructor(val service: BartService, val stationDAO: StationDao, val executor: Executor) {

    fun getStation(abbr: String): LiveData<StationXmlResponse> { // todo: if cached != null, return cached
        val data = MutableLiveData<StationXmlResponse>()
        service.getStation(abbr).enqueue(object : Callback<StationXmlResponse> {
            override fun onResponse(call: Call<StationXmlResponse>, response: Response<StationXmlResponse>) {
                data.postValue(response.body())
                Log.d(TAG, response.message())
            }

            override fun onFailure(call: Call<StationXmlResponse>, t: Throwable) {
                Log.e(TAG, "Failure: ${t.message}")
            }
        })
        return data
    }

    val stations: LiveData<StationXmlResponse>
        get() {
            val data = MutableLiveData<StationXmlResponse>()

            service.getAllStations().enqueue(object : Callback<StationXmlResponse> {
                override fun onResponse(call: Call<StationXmlResponse>, response: Response<StationXmlResponse>) {
                    data.postValue(response.body())
                    Log.d(TAG, response.message())
                }

                override fun onFailure(call: Call<StationXmlResponse>, t: Throwable) {
                    Log.e(TAG, "Failure: ${t.message}")
                }
            })
            return data
        }

    /**
     * Initialize the sqlite db with a list of stations.
     * Should be run off main thread
     */
    fun loadStations(context: Context) {
        if (NetworkUtils.isNetworkActive(context)) {
            service.getAllStations().enqueue(object : Callback<StationXmlResponse> {
                override fun onResponse(call: Call<StationXmlResponse>, response: Response<StationXmlResponse>) {
                    response.body()?.stationList?.let { list ->
                        saveStations(list)
                    }
                    Log.d(TAG, response.message())
                }

                override fun onFailure(call: Call<StationXmlResponse>, t: Throwable) {
                    Log.e(TAG, "Failure: ${t.message}")
                }
            })
        } else {
            getStationsFromFile(context)
        }
    }

    private fun getStationsFromFile(context: Context): List<Station>? {
        val `is`: InputStream?
        var stationList: List<Station>? = null
        try {
            `is` = context.assets?.open("stations.xml")
            `is`?.let {
                val parser = StationXmlParser(`is`)
                stationList = parser.list
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: XmlPullParserException) {
            e.printStackTrace()
        }

        return stationList
    }

    private fun saveStations(list: List<Station>) {
        executor.execute {
            for (station in list) {
                Log.i(TAG, station.name ?: "null station name")
                stationDAO.save(station)
            }
        }
    }

    /**
     * Update a station and its data
     */
    fun refreshStation(name: String?) {
        name ?: return

        executor.execute {
            val stationData = stationDAO.getStationByName(name)?.intro
            val stationDataExists = !stationData.isNullOrBlank()
            if (!stationDataExists) { // refresh data
                try {
                    val response = service.getStation(name).execute()
                    Logger.d("Successfully refreshed station: $name")
                    if (response.isSuccessful) {
                        response.body()?.stationList?.let { list ->
                            for (station in list) {
                                Logger.d("Updating station: $station")
                                stationDAO.save(station)
                            }
                        }
                    }
                } catch (ioe: IOException) {
                    ioe.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    companion object {
        private const val TAG = "StationRepository"
    }
}
