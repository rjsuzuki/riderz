package com.zuk0.gaijinsmash.riderz.data.local.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite
import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.BsaXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station
import com.zuk0.gaijinsmash.riderz.data.local.entity.trip.Trip
import com.zuk0.gaijinsmash.riderz.data.local.room.converter.Converters
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.BsaDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.EtdDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.FavoriteDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.StationDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.TripDao

@Database(entities = [BsaXmlResponse::class, EtdXmlResponse::class, Favorite::class, Station::class, Trip::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class RiderzDatabase : RoomDatabase() {

    abstract fun bsaDao(): BsaDao
    abstract fun etdDao(): EtdDao
    abstract fun favoriteDao(): FavoriteDao
    abstract fun stationDao(): StationDao
    abstract fun tripDao(): TripDao

    companion object {
        private var INSTANCE: RiderzDatabase? = null
        fun getRoomDb(context: Context): RiderzDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, RiderzDatabase::class.java, "riderz.db")
                    .addMigrations(MIGRATION)
                    .build()
            }
            return INSTANCE as RiderzDatabase
        }

        // Edit this to create a new migration for database - and use ".addMigrations(example)
        private val MIGRATION: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // database.execSQL("ALTER TABLE advisories ADD COLUMN last_update INTEGER")
            }
        }
    }
}
