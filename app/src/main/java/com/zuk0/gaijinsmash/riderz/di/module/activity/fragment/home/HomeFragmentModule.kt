package com.zuk0.gaijinsmash.riderz.di.module.activity.fragment.home

import dagger.Module

@Module
class HomeFragmentModule {
    // home fragment dependencies here
}
