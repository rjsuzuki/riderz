package com.zuk0.gaijinsmash.riderz.data.local.manager

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.utils.LogUtil

class LocationManager(context: Context) : LocationListener, LifecycleObserver {

    var location: Location?

    private var latitude: Double = 0.toDouble()
    private var longitude: Double = 0.toDouble()
    private val locationProvider: String

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        // todo
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        // cache location
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        // get cache from bundle?
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        location = null
    }

    init {
        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE // Define the criteria for how to select the location provider

        locationProvider = initLocationProvider(context, manager, criteria)
        location = getLocation(manager, context)

        // Initialize the location fields
        location?.let { onLocationChanged(it) } ?: Log.i("GPS:", "Location is not available")
    }

    private fun initLocationProvider(context: Context, manager: LocationManager?, criteria: Criteria): String {
        var locationProvider = ""
        if (checkLocationPermission(context)) {
            if (manager != null) {
                locationProvider = manager.getBestProvider(criteria, true) as String
                Log.i(TAG, locationProvider)
            }
        }
        return locationProvider
    }

    private fun getLocation(manager: LocationManager, context: Context): Location? {
        var location: Location? = null
        if (checkLocationPermission(context)) {
            location = manager.getLastKnownLocation(locationProvider)
            if (location == null) {
                location = tryAllProviders(manager, context)
            }
        }
        return location
    }

    private fun tryAllProviders(manager: LocationManager, context: Context): Location? {
        var location: Location? = null
        if (checkLocationPermission(context)) {
            for (provider in manager.allProviders) {
                location = manager.getLastKnownLocation(provider)
                return location ?: continue
            }
        }
        return location
    }

    override fun onLocationChanged(location: Location) {
        this.location = location
        latitude = location.latitude
        longitude = location.longitude
    }

    override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {
        Logger.d(s)
    }

    override fun onProviderEnabled(s: String) {
        Logger.d(s)
    }

    override fun onProviderDisabled(s: String) {
        Logger.d(s)
    }

    companion object {
        private const val TAG = "LocationManager"

        const val LOCATION_PERMISSION_REQUEST_CODE = 121
        fun checkLocationPermission(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        }

        fun checkIfExplanationIsNeeded(activity: Activity?): Boolean {
            LogUtil.log(Log.DEBUG, TAG, "checking location permissions...")
            if (activity == null) {
                return false
            }

            return ActivityCompat.shouldShowRequestPermissionRationale(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION,
            )
        }

        fun showPermissionExplanation(activity: Activity?) {
            activity ?: return

            LogUtil.log(Log.DEBUG, TAG, "showing AlertDialog to explain permission request")
            val alert = AlertDialog.Builder(activity)
                .setTitle(activity.resources.getString(R.string.title_permission_location))
                .setMessage(activity.resources.getString(R.string.explanation_location))
                .setPositiveButton(activity.resources.getString(R.string.accept)) { dialog, _ ->
                    val intent = Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", activity.packageName, null),
                    )
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                    activity.startActivityForResult(intent, LOCATION_PERMISSION_REQUEST_CODE)
                    dialog.dismiss()
                }
                .setCancelable(true)
                .setNegativeButton(activity.resources.getString(R.string.deny)) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
            alert.show()
        }

        fun requestPermissions(activity: Activity?) {
            activity?.let {
                LogUtil.log(Log.DEBUG, TAG, "requesting location permissions")
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE,
                )
            }
        }
    }
} // End of Class
