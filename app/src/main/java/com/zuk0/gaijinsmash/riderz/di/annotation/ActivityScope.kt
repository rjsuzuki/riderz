package com.zuk0.gaijinsmash.riderz.di.annotation

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope
