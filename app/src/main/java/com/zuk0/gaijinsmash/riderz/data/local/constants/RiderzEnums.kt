package com.zuk0.gaijinsmash.riderz.data.local.constants

class RiderzEnums {
    enum class FavoritesAction {
        ADD_FAVORITE, DELETE_FAVORITE, ADD_PRIORITY, DELETE_PRIORITY, CHECK_PRIORITY
    }
}
