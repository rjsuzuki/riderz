package com.zuk0.gaijinsmash.riderz.di.module.activity

import dagger.Module

/**
 * place all activity scoped injections here
 */
@Module
class ActivityModule
