package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.favorite

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.constants.RiderzEnums
import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite
import com.zuk0.gaijinsmash.riderz.databinding.ListRowFavorites2Binding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.trip.TripFragment

/**
 * ViewHolders already provide functionality to RecyclerView Adapter properties, like item position and viewType
 */
class FavoriteViewHolder(val binding: ListRowFavorites2Binding, val adapter: FavoriteRecyclerAdapter, val vm: FavoritesViewModel) : RecyclerView.ViewHolder(binding.root) {

    lateinit var favorite: Favorite

    /**
     * Bind data object to viewHolder
     * @param Favorite
     */
    fun bind(favorite: Favorite) {
        this.favorite = favorite
        initLayout()
        initButtonListeners()
    }

    private fun initLayout() {
        binding.departStation.text = favorite.a?.name
        binding.arriveStation.text = favorite.b?.name
        if (favorite.priority == Favorite.Priority.ON) {
            // binding.background.setBackgroundColor(binding.root.context.resources.getColor(R.color.primaryLightColor))
        }
    }

    private fun initButtonListeners() {
        binding.settingsButton.setOnClickListener { v -> displayPopupMenu(v.context, binding.settingsButton, favorite, adapterPosition) }
        binding.switchButton.setOnClickListener { v -> reverseRoute(binding.departStation, binding.arriveStation) }
        binding.getTripButton.setOnClickListener { v -> initTripSearch(binding.departStation, binding.arriveStation) }
    }

    private fun displayPopupMenu(context: Context, ib: ImageButton, favorite: Favorite?, rowPosition: Int) {
        val popupMenu = PopupMenu(context, ib)
        popupMenu.menuInflater.inflate(R.menu.favorite_options, popupMenu.menu)
        popupMenu.show()
        if (favorite != null) {
            if (favorite.priority === Favorite.Priority.ON) {
                Log.i("PRIORITY", "ON")
                val deletePriorityItem = popupMenu.menu.findItem(R.id.action_favorite_deletePriority)
                deletePriorityItem.isVisible = true
            } else {
                val setPriorityItem = popupMenu.menu.findItem(R.id.action_favorite_setPriority)
                setPriorityItem.isVisible = true
            }
        }
        popupMenu.setOnMenuItemClickListener { item ->
            selectFavoriteAction(popupMenu, context, item.itemId, favorite, rowPosition)
            true
        }
    }

    private fun selectFavoriteAction(menu: PopupMenu, context: Context, itemId: Int, favorite: Favorite?, rowPosition: Int) {
        if (favorite == null) {
            // todo
            return
        }
        when (itemId) {
            R.id.action_delete_favorite -> {
                vm.executeFavoritesAction(favorite, RiderzEnums.FavoritesAction.DELETE_FAVORITE)
                menu.dismiss()
                Toast.makeText(context, R.string.deletion_success, Toast.LENGTH_SHORT).show()
            }
            R.id.action_favorite_setPriority -> {
                vm.executeFavoritesAction(favorite, RiderzEnums.FavoritesAction.ADD_PRIORITY)
                menu.dismiss()
            }
            R.id.action_favorite_deletePriority -> {
                vm.executeFavoritesAction(favorite, RiderzEnums.FavoritesAction.DELETE_PRIORITY)
                menu.dismiss()
                Toast.makeText(context, R.string.priority_deleted, Toast.LENGTH_SHORT).show()
            }
        }
    }

    /*
        Note: pulls text directly from textviews, which are in Abbreviated format
        Must get full stations names for sql query in favorites database.
    */
    private fun initTripSearch(origin: TextView, destination: TextView) {
        Log.d(TAG, "starting trip search")
        val originString = origin.text.toString()
        val destinationString = destination.text.toString()

        val bundle = Bundle()
        bundle.putString(TripFragment.TripBundle.ORIGIN.value, originString)
        bundle.putString(TripFragment.TripBundle.DESTINATION.value, destinationString)
        bundle.putString(TripFragment.TripBundle.DATE.value, "TODAY")
        bundle.putString(TripFragment.TripBundle.TIME.value, "NOW")
        bundle.putBoolean("FAVORITE_RECYCLER_ADAPTER", true)

        Navigation.findNavController(binding.root).navigate(R.id.action_favoritesFragment_to_resultsFragment, bundle, null, null)
    }

    private fun reverseRoute(origin: TextView, destination: TextView) {
        val temp = origin.text.toString()
        origin.text = destination.text
        destination.text = temp
    }

    companion object {
        private const val TAG = "FavoriteViewHolder"
    }
}
