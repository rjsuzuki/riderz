package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite
import com.zuk0.gaijinsmash.riderz.databinding.FragmentFavoritesBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment
import com.zuk0.gaijinsmash.riderz.utils.KeyboardUtils
import com.zuk0.gaijinsmash.riderz.utils.StationUtils
import javax.inject.Inject

class FavoritesFragment : BaseFragment() {

    @Inject
    lateinit var mFavoritesViewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentFavoritesBinding
    private lateinit var viewModel: FavoritesViewModel
    private var adapter: FavoriteRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFavoritesBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.collapseAppBar(activity)
        super.setTitle(activity, getString(R.string.favorites))
    }

    override fun onResume() {
        super.onResume()
        initFavorites()
        handleCardView()
        viewModel.isFavoriteCardViewLiveData.observe(
            viewLifecycleOwner,
            Observer { result ->
                if (result) {
                    binding.bartFavoritesCardView.visibility = View.VISIBLE
                } else {
                    binding.bartFavoritesCardView.visibility = View.GONE
                }
            },
        )
        viewModel.isAdapterDataChanged.observe(
            viewLifecycleOwner,
            Observer { list ->
                adapter?.updateList(list as MutableList<Favorite>?)
                if (list.isNullOrEmpty()) {
                    binding.favoritesAddBtn.visibility = View.GONE
                }
            },
        )
        binding.favoritesAddBtn.setOnClickListener {
            binding.bartFavoritesCardView.visibility = View.VISIBLE
            binding.cancelButton.visibility = View.VISIBLE
            binding.favoritesAddBtn.visibility = View.GONE
            binding.bartFavoritesErrorTV.visibility = View.GONE
        }
        binding.cancelButton.setOnClickListener {
            binding.favoritesAddBtn.visibility = View.VISIBLE
            binding.cancelButton.visibility = View.GONE
            binding.bartFavoritesCardView.visibility = View.GONE
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, mFavoritesViewModelFactory).get(FavoritesViewModel::class.java)
    }

    private fun initFavorites() {
        viewModel.favorites?.observe(
            viewLifecycleOwner,
            Observer { data ->
                if (data.isNotEmpty()) {
                    binding.bartFavoritesCardView.visibility = View.GONE
                    adapter = FavoriteRecyclerAdapter(data, viewModel)
                    binding.bartFavoritesRecyclerView.adapter = adapter
                    binding.bartFavoritesRecyclerView.layoutManager = LinearLayoutManager(activity)
                    binding.favoritesAddBtn.visibility = View.VISIBLE
                }
            },
        )
    }

    private fun handleCardView() {
        val adapter = ArrayAdapter<String>(requireContext(), android.R.layout.select_dialog_item, resources.getStringArray(R.array.stations_list))
        binding.favoriteDepartAutoCompleteTextView.setAdapter(adapter)
        binding.favoriteDepartAutoCompleteTextView.threshold = 1
        binding.favoriteDepartAutoCompleteTextView.setOnDismissListener {
            KeyboardUtils.closeKeyboard(activity)
        }
        binding.favoriteArrivalAutoCompleteTextView.setAdapter(adapter)
        binding.favoriteArrivalAutoCompleteTextView.threshold = 1
        binding.favoriteArrivalAutoCompleteTextView.setOnDismissListener {
            KeyboardUtils.closeKeyboard(activity)
        }
        binding.button.setOnClickListener { v ->
            validate()
        }
    }

    private fun validate() {
        val depart = binding.favoriteDepartAutoCompleteTextView.text.toString()
        val arrive = binding.favoriteArrivalAutoCompleteTextView.text.toString()
        if (depart.isBlank()) {
            binding.favoriteDepartAutoCompleteTextView.error = resources.getString(R.string.error_field_incomplete)
            return
        }
        if (arrive.isBlank()) {
            binding.favoriteArrivalAutoCompleteTextView.error = resources.getString(R.string.error_field_incomplete)
            return
        }
        if (!StationUtils.validateStationName(depart)) {
            binding.favoriteDepartAutoCompleteTextView.error = resources.getString(R.string.error_station_not_found)
            return
        }
        if (!StationUtils.validateStationName(arrive)) {
            binding.favoriteArrivalAutoCompleteTextView.error = resources.getString(R.string.error_station_not_found)
            return
        }
        viewModel.saveFavorite(depart, arrive)
    }
}
