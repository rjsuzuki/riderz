package com.zuk0.gaijinsmash.riderz.ui.activity.main

import android.app.Application
import android.content.Context
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.zuk0.gaijinsmash.riderz.BuildConfig
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.weather.WeatherResponse
import com.zuk0.gaijinsmash.riderz.data.local.manager.LocationManager
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase
import com.zuk0.gaijinsmash.riderz.data.remote.repository.WeatherRepository
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.settings.SettingsFragment
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper
import com.zuk0.gaijinsmash.riderz.utils.TimeDateUtils
import java.util.*
import javax.inject.Inject

class MainViewModel
@Inject constructor(application: Application, val weatherRepository: WeatherRepository, val db: RiderzDatabase) : AndroidViewModel(application) {

    private val userLocation: Location? = null

    val isDaytime: Boolean
        get() = TimeDateUtils.isDaytime

    val isNightTime: Boolean
        get() = TimeDateUtils.isNightTime

    val isDuskOrDawn: Boolean
        get() = TimeDateUtils.isDusk or TimeDateUtils.isMorning

    internal val hour: Int
        get() = TimeDateUtils.currentHour

    private val sharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(getApplication())
    }

    fun getVersionCode(): String {
        return "v${BuildConfig.VERSION_NAME}"
    }

    fun isMetricSystemEnabled(): Boolean {
        return sharedPreferences.getBoolean(SettingsFragment.PreferenceKey.METRIC_PREFS.value, false)
    }

    fun getLocation(): Location? {
        if (userLocation == null) {
            val gps = LocationManager(getApplication())
            gps.location
        }
        return userLocation
    }

    /**
     * @return colorResourceId
     * @param context
     * @param hour
     */
    fun getTitleColor(context: Context, hour: Int): Int {
        return if (hour < 6 || hour >= 20) {
            // show night color
            ContextCompat.getColor(context, R.color.white)
        } else if (hour in 18..19) {
            // show dusk color
            ContextCompat.getColor(context, R.color.white)
        } else {
            // show day color
            ContextCompat.getColor(context, R.color.white)
        }
    }

    /**
     * @return a drawable gradient for the AppBar
     * @param context
     * @param hour
     */
    fun getBackgroundDrawable(context: Context, hour: Int): Drawable? {
        return if (hour < 6 || hour >= 21) {
            // show night picture
            ContextCompat.getDrawable(context, R.drawable.gradient_night)
        } else if (hour >= 17) {
            // show dusk picture
            ContextCompat.getDrawable(context, R.drawable.gradient_dusk)
        } else {
            ContextCompat.getDrawable(context, R.drawable.gradient_day)
        }
    }

    /**
     * @return color int value based on the time of day
     * @param hour
     * @param isAppBarOpen
     */
    fun getStatusBarColor(hour: Int, isAppBarOpen: Boolean): Int {
        return if (hour < 6 || hour >= 21) { // night
            if (isAppBarOpen) {
                ResourcesCompat.getColor(getApplication<Application>().resources, R.color.nighttime_dark, getApplication<Application>().theme)
            } else {
                ResourcesCompat.getColor(getApplication<Application>().resources, R.color.colorPrimary, getApplication<Application>().theme)
            }
        } else if (hour >= 17) { // dusk
            if (isAppBarOpen) {
                ResourcesCompat.getColor(getApplication<Application>().resources, R.color.dusktime_dark, getApplication<Application>().theme)
            } else {
                ResourcesCompat.getColor(getApplication<Application>().resources, R.color.colorPrimary, getApplication<Application>().theme)
            }
        } else { // day
            if (isAppBarOpen) {
                ResourcesCompat.getColor(getApplication<Application>().resources, R.color.daytime_dark, getApplication<Application>().theme)
            } else {
                ResourcesCompat.getColor(getApplication<Application>().resources, R.color.colorPrimary, getApplication<Application>().theme)
            }
        }
    }

    /**
     * Save UI state
     * @param outState
     */
    fun saveState(outState: Bundle) {
        // todo
    }

    /**
     * Restore UI state
     * @param savedInstanceState
     */
    fun restoreState(savedInstanceState: Bundle) {
        // todo
    }

    /**
     * @return converted MPH to KPH
     * @param mph
     * This is not rounded and requires string formatting
     * so that you don't print out a value like, 2.3320000000002
     */
    fun toKPH(mph: Double?): Double {
        mph?.let {
            return (mph * 1.60934)
        }
        return 0.0
    }

    /**
     * Convert metric KPH to imperial MPH
     * @return the [Double] value of the parameter in miles per/hour (mph)
     * @param kph
     */
    fun toMPH(kph: Double?): Double {
        kph?.let {
            return (kph / 1.60934)
        }
        return 0.0
    }

    /**
     * @return a formatted String of the parameter
     * @param value
     */
    fun formatDoubleValue(value: Double): String {
        return String.format("%.1f", value)
    }

    /**
     * @return a [LiveData] object of a Weather response object
     * @see [LiveDataWrapper]
     */
    fun getWeather(): LiveData<LiveDataWrapper<WeatherResponse>> {
        userLocation?.let {
            return weatherRepository.getWeatherByGeoloc(it.latitude, it.longitude)
        }
        return weatherRepository.getWeatherByZipcode(DEFAULT_ZIPCODE) // default if userLocation is unavailable
    }

    /**
     * @return converted kelvin temperature to Fahrenheit
     * @param temp
     * F = 9/5 (K - 273) + 32
     */
    fun kelvinToFahrenheit(temp: Double): Double {
        return 9f / 5f * (temp - 273) + 32
    }

    /**
     * @return converted Kelvin temperature to Celcius
     * @param temp
     * C = K - 273
     */
    internal fun kelvinToCelcius(temp: Double): Double {
        return temp - 273
    }

    /**
     * @return converted fahrenheit temperature to Celcius
     * @param temp
     */
    internal fun fahrenheightToCelcius(temp: Double): Double {
        return (temp - 32) * 5 / 9
    }

    /**
     * @return converted celcius temperature to Fahrenheit
     * @param temp
     */
    internal fun celciusToFahrenheit(temp: Double): Double {
        return temp * 9 / 5 + 32
    }

    /**
     * @param context
     * @param imageView
     */
    fun initBartMap(context: Context?, imageView: ImageView?) {
        if (context != null && imageView != null) {
            val img: Drawable?
            val cal = Calendar.getInstance(TimeZone.getTimeZone("PST"), Locale.US)
            val day = cal.get(Calendar.DAY_OF_WEEK)
            if (day == 7) {
                img = context.getDrawable(R.drawable.bart_map_sunday)
            } else {
                img = context.getDrawable(R.drawable.bart_map_weekday_sat)
            }
            Glide.with(context)
                .load(img)
                .into(imageView)
        }
    }

    companion object {
        private const val TAG = "MainViewModel"
        private const val DEFAULT_ZIPCODE = 94108 // San Francisco
    }
}
