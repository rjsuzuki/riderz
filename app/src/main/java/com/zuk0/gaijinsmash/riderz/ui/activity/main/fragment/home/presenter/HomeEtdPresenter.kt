package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.presenter

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station
import com.zuk0.gaijinsmash.riderz.databinding.PresenterEstimateBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.HomeViewModel
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.CustomSpinnerAdapter
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.etd.PlatformRecyclerAdapter
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stationinfo.StationInfoFragment
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper
import com.zuk0.gaijinsmash.riderz.utils.TimeDateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeEtdPresenter(
    val etdBinding: PresenterEstimateBinding,
    val viewModel: HomeViewModel,
    lifecycleScope: LifecycleCoroutineScope,
) : DefaultLifecycleObserver {

    init {
        Log.d(TAG, "init")
    }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        Log.d(TAG, "onCreate")
        initStationInfoButton()
        initSpinner()
        initRefreshButton()
    }

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        Log.d(TAG, "onStart")
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        Log.d(TAG, "onResume")
        viewModel.isEtdRefreshNeeded.observeForever { refresh ->
            if (refresh) {
                refreshLists()
                viewModel.timer.resume()
            }
        }
        viewModel.timer.resume()
    }

    override fun onPause(owner: LifecycleOwner) {
        Log.d(TAG, "onPause")
        viewModel.timer.paused()
        super.onPause(owner)
    }

    override fun onStop(owner: LifecycleOwner) {
        Log.d(TAG, "onStop")
        super.onStop(owner)
    }

    override fun onDestroy(owner: LifecycleOwner) {
        Log.d(TAG, "onDestroy")
        try {
            viewModel.getNearestStation(viewModel.getLocation()).removeObserver(stationObserver)
            viewModel.station?.let {
                viewModel.getEstimatesLiveData(it).removeObserver(estimatesObserver)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        viewModel.timer.destroy()
        viewModel.viewModelScope.cancel()
        super.onDestroy(owner)
    }

    private fun initStationInfoButton() {
        etdBinding.etdStationInfoIcon.setOnClickListener {
            // get station
            viewModel.selectedStation?.name?.let {
                viewModel.navigationLiveData.postValue(StationInfoFragment.TAG)
            }
        }
    }

    private fun initRefreshButton() {
        etdBinding.etdRefreshButton.setOnClickListener {
            viewModel.refreshCount++
            if (viewModel.refreshCount >= viewModel.refreshMax) {
                Toast.makeText(viewModel.getApplication(), viewModel.getApplication<Application>().resources.getString(R.string.too_many_requests), Toast.LENGTH_SHORT).show()
            } else {
                refreshLists()
            }
        }
    }

    private fun refreshLists() {
        initEstimateListsByPlatform()
    }

    private fun initSpinner() {
        viewModel.viewModelScope.launch {
            val stationsTask = withContext(Dispatchers.IO) {
                viewModel.getStationsFromDb()
            }

            val adapter2 = CustomSpinnerAdapter(viewModel.getApplication(), R.layout.custom_dropdown_item2, stationsTask)

            etdBinding.etdSpinner.adapter = adapter2

            etdBinding.etdSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    Log.d(TAG, "onNothingSelected()")
                    viewModel.selectedStation?.let {
                        etdBinding.etdStationButton.text = viewModel.selectedStation?.name
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    Log.d(TAG, "onItemSelected()")
                    viewModel.selectedStation = etdBinding.etdSpinner.selectedItem as Station
                    viewModel.selectedStation?.let {
                        etdBinding.etdStationButton.text = viewModel.selectedStation?.name
                        refreshLists()
                    }
                }
            }
            etdBinding.etdStationButton.text = viewModel.selectedStation?.name
            initStation() // start here to prevent potential NPEs from race conditions
        }
    }

    private fun initStation() {
        viewModel.getNearestStation(viewModel.getLocation()).observeForever(stationObserver)
        etdBinding.etdStationButton.setOnClickListener {
            etdBinding.etdSpinner.performClick()
        }
    }

    private val stationObserver: Observer<Station> = Observer {
        etdBinding.etdStationButton.text = it.name ?: ""
        viewModel.selectedStation = it
        val selectedPosition = (etdBinding.etdSpinner.adapter as CustomSpinnerAdapter).getPosition(it)
        etdBinding.etdSpinner.setSelection(selectedPosition)
        refreshLists()
    }

    private fun initEstimateListsByPlatform() {
        viewModel.selectedStation?.let { station ->
            viewModel.station = station
            viewModel.getEstimatesLiveData(station).observeForever(estimatesObserver)
        }
    }

    private val estimatesObserver: Observer<LiveDataWrapper<EtdXmlResponse>> = Observer { data ->
        when (data.status) {
            LiveDataWrapper.Status.SUCCESS -> {
                if (data.data.station?.etdList != null) {
                    data.data.timestamp?.let {
                        viewModel.cachedTimeDifference = viewModel.getMinutesPassedFromCache(it)
                    }
                    val map = viewModel.createEstimateListsByPlatform2(data.data.station?.etdList)
                    val list = viewModel.sortMapIntoList(map)
                    etdBinding.homeEtdRv.layoutManager = LinearLayoutManager(viewModel.getApplication())
                    viewModel.platformRecyclerAdapter = PlatformRecyclerAdapter(list, viewModel)
                    etdBinding.homeEtdRv.adapter = viewModel.platformRecyclerAdapter
                } else if (TimeDateUtils.isAfterHours) {
                    showAfterHoursLayout()
                }
            }
            LiveDataWrapper.Status.ERROR -> {
                data.msg?.let { Logger.e(it) }
                showErrorLayout()
            }
            LiveDataWrapper.Status.LOADING -> {
                Logger.i("loading")
            }
        }
    }

    private fun showAfterHoursLayout() {
        // if between 0100 and 0600 and etd == null or empty
        etdBinding.etdRecyclerContainer.visibility = View.GONE
        etdBinding.afterHoursLayout.root.visibility = View.VISIBLE
    }

    private fun showErrorLayout() {
        etdBinding.errorLayout.root.visibility = View.VISIBLE
    }

    companion object {
        private const val TAG = "HomeEtdPresenter"
    }
}
