package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.help

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.databinding.FragmentHelpBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment

class HelpFragment : BaseFragment() {

    private lateinit var binding: FragmentHelpBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentHelpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.collapseAppBar(activity)
        super.setTitle(activity, getString(R.string.help_title))
        binding.helpReportButton.setOnClickListener { launchEmailIntent() }
    }

    private fun launchEmailIntent() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.setType("message/rfc822")
            .putExtra(Intent.EXTRA_EMAIL, arrayOf(RIDERZ_SUPPORT_CONTACT))
            .putExtra(Intent.EXTRA_SUBJECT, "***RIDERZ ISSUE REPORT***")
            .putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.help_intent_text))
        startActivity(Intent.createChooser(intent, resources.getString(R.string.help_intent_title)))
    }

    companion object {
        const val TAG = "HelpFragment"
        private const val RIDERZ_SUPPORT_CONTACT = "contact-project+rjsuzuki-riderz-support@incoming.gitlab.com"
    }
}
