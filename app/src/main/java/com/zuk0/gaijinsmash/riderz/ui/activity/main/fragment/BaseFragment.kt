package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.ui.activity.main.MainActivity
import dagger.android.support.AndroidSupportInjection

@Deprecated("Refactor with kotlin extensions")
abstract class BaseFragment : Fragment() {

    // ---------------------------------------------------------------------------------------------
    // Lifecycle Events
    // ---------------------------------------------------------------------------------------------

    override fun onAttach(context: Context) {
        initDagger() // this must go before the super method
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showFab(true)
    }

    // ---------------------------------------------------------------------------------------------
    // Helpers
    // ---------------------------------------------------------------------------------------------

    private fun initDagger() {
        AndroidSupportInjection.inject(this)
    }

    /**
     * @param activity
     */
    fun collapseAppBar(activity: Activity?) {
        activity?.let {
            val appBarLayout: AppBarLayout? = it.findViewById(R.id.main_app_bar_layout)
            appBarLayout?.setExpanded(false)
        }
    }

    /**
     * @param activity
     */
    fun expandAppBar(activity: Activity?) {
        activity?.let {
            val appBarLayout: AppBarLayout? = it.findViewById(R.id.main_app_bar_layout)
            appBarLayout?.setExpanded(true)
        }
    }

    /**
     * if [isClosed] is true, the view will slide open. if false, the view will slide closed.
     * @param isClosed - the current state of the view before a change is applied.
     */
    fun toggleBottomNavigation(isClosed: Boolean) {
        activity?.let {
            val bottomNav: BottomNavigationView? = it.findViewById(R.id.main_bottom_navigation)
            bottomNav?.layoutParams?.let { bottomNavLayoutParams ->
                if (bottomNavLayoutParams is CoordinatorLayout.LayoutParams) {
                    if (bottomNavLayoutParams.behavior != null && bottomNavLayoutParams.behavior is HideBottomViewOnScrollBehavior) {
                        if (isClosed) {
                            (bottomNavLayoutParams.behavior as HideBottomViewOnScrollBehavior<View>).slideUp(bottomNav)
                        } else {
                            (bottomNavLayoutParams.behavior as HideBottomViewOnScrollBehavior<View>).slideDown(bottomNav)
                        }
                    }
                }
            }
        }
    }

    /**
     * @param activity
     * @param title
     */
    fun setTitle(activity: Activity?, title: String) {
        activity?.let {
            val collapsingToolbarLayout: CollapsingToolbarLayout? = activity.findViewById(R.id.main_collapsing_toolbar)
            collapsingToolbarLayout?.title = title
        }
    }
}
