package com.zuk0.gaijinsmash.riderz.data.local.entity.trip

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Fares {
    @SerializedName("@level")
    @Expose
    var level: String? = null

    @SerializedName("fare")
    @Expose
    var fareList: List<Fare>? = null
}
