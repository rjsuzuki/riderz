package com.zuk0.gaijinsmash.riderz.di.module.activity.fragment

import com.zuk0.gaijinsmash.riderz.di.annotation.FragmentScope
import com.zuk0.gaijinsmash.riderz.di.module.activity.fragment.home.HomeFragmentModule
import com.zuk0.gaijinsmash.riderz.di.module.activity.fragment.stations.StationsModule
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.about.AboutFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.bartmap.BartMapFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.bartresults.BartResultsFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.camera.CameraFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.favorite.FavoritesFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.googlemap.GoogleMapFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.help.HelpFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.HomeFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.phonelines.PhoneLinesFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stationinfo.StationInfoFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stations.StationsFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.trip.TripFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.webview.WebviewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeWebviewFragment(): WebviewFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeHelpFragment(): HelpFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeAboutFragment(): AboutFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    fun contributeHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeStationInfoFragment(): StationInfoFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [StationsModule::class])
    fun contributeStationsFragment(): StationsFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeTripFragment(): TripFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeBartResultsFragment(): BartResultsFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeGoogleMapFragment(): GoogleMapFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeBartMapFragmentFragment(): BartMapFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeFavoritesFragmentt(): FavoritesFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributePhoneLinesFragment(): PhoneLinesFragment

    @FragmentScope
    @ContributesAndroidInjector
    fun contributeCameraFragment(): CameraFragment
}
