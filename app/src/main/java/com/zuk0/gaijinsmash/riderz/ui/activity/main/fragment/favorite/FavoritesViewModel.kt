package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.favorite

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zuk0.gaijinsmash.riderz.data.local.constants.RiderzEnums
import com.zuk0.gaijinsmash.riderz.data.local.entity.Favorite
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoritesViewModel
@Inject constructor(application: Application, val db: RiderzDatabase) : AndroidViewModel(application) {

    val isFavoriteCardViewLiveData = MutableLiveData<Boolean>()
    val isAdapterDataChanged = MutableLiveData<List<Favorite>>()

    val favorites: LiveData<MutableList<Favorite>>?
        get() = db?.favoriteDao()?.allFavoritesLiveData

    fun saveFavorite(depart: String, arrive: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val fav = Favorite()
            val taskA = async {
                db?.stationDao()?.getStationByName(depart)
            }
            val taskB = async {
                db?.stationDao()?.getStationByName(arrive)
            }
            fav.a = taskA.await()
            fav.b = taskB.await()
            db?.favoriteDao()?.save(fav)
        }
    }

    fun executeFavoritesAction(favorite: Favorite, action: RiderzEnums.FavoritesAction) {
        viewModelScope.launch(Dispatchers.IO) {
            when (action) {
                RiderzEnums.FavoritesAction.ADD_PRIORITY -> {
                    db?.favoriteDao()?.priorityCount?.let { count ->
                        when {
                            count > 0 -> {
                                // do nothing
                                false
                            }
                            count == 0 -> {
                                db?.favoriteDao()?.updatePriorityById(favorite.id)
                                isAdapterDataChanged.postValue(db?.favoriteDao()?.list)
                                true
                            }
                            else -> true
                        }
                    }
                }
                RiderzEnums.FavoritesAction.DELETE_PRIORITY -> {
                    db?.favoriteDao()?.removePriorityById(favorite.id)
                    isAdapterDataChanged.postValue(db?.favoriteDao()?.list)
                }
                RiderzEnums.FavoritesAction.DELETE_FAVORITE -> {
                    db?.favoriteDao()?.delete(favorite)
                    isAdapterDataChanged.postValue(db?.favoriteDao()?.list)
                }
                else -> Log.w(TAG, "unhandled  favorite action")
            }
        }
    }

    companion object {
        private const val TAG = "FavoritesViewModel"
    }
}
