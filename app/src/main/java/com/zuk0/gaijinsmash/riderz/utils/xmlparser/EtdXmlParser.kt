package com.zuk0.gaijinsmash.riderz.utils.xmlparser

import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.Etd
import java.io.InputStream

class EtdXmlParser(inputStream: InputStream) : BaseXmlParser<Etd>(inputStream, "station")
