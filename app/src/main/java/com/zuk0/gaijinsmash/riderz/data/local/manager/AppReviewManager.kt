package com.zuk0.gaijinsmash.riderz.data.local.manager

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.zuk0.gaijinsmash.riderz.utils.LogUtil
import com.zuk0.gaijinsmash.riderz.utils.TimeDateUtils
import java.sql.Date

/**
 * [In App Review Docs](https://developer.android.com/guide/playcore/in-app-review/kotlin-java#kotlin)
 */
object AppReviewManager {

    private const val TAG = "AppReviewManager"

    enum class FlowState { PREPARE, OPEN, CLOSE }
    enum class ReviewState { UNKNOWN, REQUEST, UNCONFIRMED, GRACE_PERIOD, CONFIRMED }
    enum class UserAction { YES, NO, IGNORE }

    private var manager: ReviewManager? = null
    private var reviewInfo: ReviewInfo? = null

    private const val INAPP_REVIEW_SHARED_PREFS_NAME = "INAPP_REVIEW_SHARED_PREFS"

    private const val SHARED_PREFS_KEY_USER_HAS_RATED = "SHARED_PREFS_KEY_USER_HAS_RATED"
    private const val SHARED_PREFS_KEY_USER_LAST_MOD_DATE = "SHARED_PREFS_KEY_USER_LAST_MOD_DATE"
    private const val SHARED_PREFS_KEY_USER_CONFIRMED = "SHARED_PREFS_KEY_USER_CONFIRMED"
    private const val SHARED_PREFS_KEY_USER_DISMISSED = "SHARED_PREFS_KEY_USER_DISMISSED"

    private var reviewState: ReviewState = ReviewState.UNKNOWN
    private var isReviewFlowStarting = false

    fun getState(): ReviewState {
        return reviewState
    }

    /**
     * Set the number of days to wait until requesting another in-app review.
     */
    private const val DURATION_OF_DAYS = 30

    /**
     * Initialize and prepare the [ReviewManager] to be launched.
     *
     * Note: The ReviewInfo object is only valid for a limited amount of time. Your app should
     * request a ReviewInfo object ahead of time (pre-cache) but only once you are certain that
     * your app will launch the in-app review flow.
     */
    fun requestAppReview(activity: Activity?) {
        activity ?: return

        LogUtil.log(Log.DEBUG, TAG, "initializing google's in-app ReviewManager...")
        try {
            if (manager == null) {
                manager = ReviewManagerFactory.create(activity)
            }
            val request = manager?.requestReviewFlow()
            request?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "reviewInfo successful")
                    // We got the ReviewInfo object
                    reviewInfo = task.result
                    launchReviewFlow(activity)
                    isReviewFlowStarting = false
                } else {
                    // There was some problem, log or handle the error code
                    task.exception?.let { LogUtil.logException(it) }
                }
            }
            request?.addOnFailureListener {
                Log.wtf(TAG, "reviewInfo failed")
                isReviewFlowStarting = false
            }
        } catch (e: Exception) {
            LogUtil.logException(e)
        }
    }

    /**
     * @return Determines whether or not the app successfully attempted to launch the review flow. A
     * successful launch does not mean the UI Review Card was successfully displayed.
     * @param activity [Activity]
     */
    private fun launchReviewFlow(activity: Activity?): Boolean {
        activity ?: return false

        try {
            reviewInfo?.let {
                isReviewFlowStarting = true
                LogUtil.log(Log.DEBUG, TAG, "launching google's in-app ReviewManager...")
                val flow = manager?.launchReviewFlow(activity, it)
                flow?.addOnCompleteListener { _ ->
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                }
                return true
            }
        } catch (e: Exception) {
            LogUtil.logException(e)
        }
        return false
    }

    /**
     * Open the device's Play Store in order to review the app
     * @param context
     */
    fun launchPlayStoreIntent(context: Context?) {
        context ?: return

        try {
            val uri = Uri.parse("market://details?id=${context.packageName}")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            context.startActivity(intent)
        } catch (e: Exception) {
            LogUtil.logException(e) // user does not have GooglePlay Store
            val uri = Uri.parse("https://play.google.com/store/apps/details?id=${context.packageName}")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    /**
     * @return true/false if the app should prompt user to review the app.
     */
    fun isReviewWanted(context: Context?): Boolean {
        if (isReviewFlowStarting) {
            return false
        }
        if (isReviewDone(context)) {
            return when {
                isReviewConfirmed(context) -> {
                    Log.d(TAG, "User already rated app")
                    reviewState = ReviewState.CONFIRMED
                    false
                }
                isGracePeriod(context) -> {
                    Log.d(TAG, "User is still within the grace period.")
                    reviewState = ReviewState.GRACE_PERIOD
                    false
                }

                else -> {
                    Log.d(TAG, "User confirmation required.")
                    reviewState = ReviewState.UNCONFIRMED
                    true
                }
            }
        } else { // first time or dismissed
            if (isReviewDismissed(context)) {
                Log.d(TAG, "User has explicitly dismissed this.")
                reviewState = ReviewState.GRACE_PERIOD
                return false
            }
            Log.d(TAG, "User has not rated app yet.")
            reviewState = ReviewState.REQUEST
            return true
        }
    }

    private fun isReviewDismissed(context: Context?): Boolean {
        context?.let {
            val prefs = context.getSharedPreferences(INAPP_REVIEW_SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            return prefs.getBoolean(SHARED_PREFS_KEY_USER_DISMISSED, false)
        }
        return false
    }

    /**
     * @return true/false if the review has potentially been submitted.
     */
    private fun isReviewDone(context: Context?): Boolean {
        context?.let {
            val prefs = context.getSharedPreferences(INAPP_REVIEW_SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            return prefs.getBoolean(SHARED_PREFS_KEY_USER_HAS_RATED, false)
        }
        return false
    }

    /**
     * @return true/false if the time of day is late enough to request an in-app review.
     * start the Review Flow.
     */
    private fun isSafeToRequestReviewAPI(context: Context?): Boolean {
        context?.let {
            val prefs = context.getSharedPreferences(INAPP_REVIEW_SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            val timeOfLastAction = prefs.getLong(SHARED_PREFS_KEY_USER_LAST_MOD_DATE, 0)
            if (timeOfLastAction > 0) {
                val timeNow = System.currentTimeMillis()
                val numOfDaysPassed = TimeDateUtils.durationOfDaysBetweenDates(Date(timeOfLastAction), Date(timeNow))
                return numOfDaysPassed <= DURATION_OF_DAYS
            }
        }
        return false
    }

    /**
     * @return true/false if the user is still within the grace period. If true, we should not
     * start the Review Flow.
     */
    private fun isGracePeriod(context: Context?): Boolean {
        context?.let {
            val prefs = context.getSharedPreferences(INAPP_REVIEW_SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            val timeOfLastAction = prefs.getLong(SHARED_PREFS_KEY_USER_LAST_MOD_DATE, 0)
            if (timeOfLastAction > 0) {
                val timeNow = System.currentTimeMillis()
                val numOfDaysPassed = TimeDateUtils.durationOfDaysBetweenDates(Date(timeOfLastAction), Date(timeNow))
                return numOfDaysPassed <= DURATION_OF_DAYS
            }
        }
        return false
    }

    /**
     * @return true/false if the user has confirmed the completion of the review.
     */
    private fun isReviewConfirmed(context: Context?): Boolean {
        context?.let {
            val prefs = context.getSharedPreferences(INAPP_REVIEW_SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            return prefs.getBoolean(SHARED_PREFS_KEY_USER_CONFIRMED, false)
        }
        return false
    }

    fun updateUserRatingAction(context: Context?, action: UserAction) {
        LogUtil.log(Log.INFO, TAG, "updating user rating action: $action")
        context?.let {
            val sharedPreferences = context.getSharedPreferences(INAPP_REVIEW_SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            when (action) {
                UserAction.NO -> {
                    sharedPreferences.edit()
                        .putLong(SHARED_PREFS_KEY_USER_LAST_MOD_DATE, System.currentTimeMillis())
                        .putBoolean(SHARED_PREFS_KEY_USER_HAS_RATED, false) // assumption because we can't actually track if user rated or not
                        .putBoolean(SHARED_PREFS_KEY_USER_CONFIRMED, false)
                        .putBoolean(SHARED_PREFS_KEY_USER_DISMISSED, true)
                        .apply()
                }
                UserAction.YES -> {
                    sharedPreferences.edit()
                        .putLong(SHARED_PREFS_KEY_USER_LAST_MOD_DATE, System.currentTimeMillis())
                        .putBoolean(SHARED_PREFS_KEY_USER_HAS_RATED, true) // assumption because we can't actually track if user rated or not
                        .putBoolean(SHARED_PREFS_KEY_USER_CONFIRMED, false)
                        .putBoolean(SHARED_PREFS_KEY_USER_DISMISSED, true)
                        .apply()
                }
                UserAction.IGNORE -> { // don't ever show again
                    sharedPreferences.edit()
                        .putLong(SHARED_PREFS_KEY_USER_LAST_MOD_DATE, System.currentTimeMillis())
                        .putBoolean(SHARED_PREFS_KEY_USER_HAS_RATED, true) // assumption because we can't actually track if user rated or not
                        .putBoolean(SHARED_PREFS_KEY_USER_CONFIRMED, true)
                        .putBoolean(SHARED_PREFS_KEY_USER_DISMISSED, true)
                        .apply()
                }
            }
        }
    }

    /**
     * Release the objects from memory to be garbage collected.
     */
    fun destroy() {
        reviewInfo = null
        manager = null
        LogUtil.log(Log.DEBUG, TAG, "destroying google's in-app ReviewManager...")
    }
}
