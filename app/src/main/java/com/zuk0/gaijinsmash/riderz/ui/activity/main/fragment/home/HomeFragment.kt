package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.BsaXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.manager.LocationManager
import com.zuk0.gaijinsmash.riderz.data.local.manager.LocationManager.Companion.LOCATION_PERMISSION_REQUEST_CODE
import com.zuk0.gaijinsmash.riderz.databinding.FragmentHomeBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.bsa.BsaRecyclerAdapter
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.presenter.HomeEtdPresenter
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stationinfo.StationInfoFragment
import com.zuk0.gaijinsmash.riderz.ui.shared.permission.PermissionPresenter
import com.zuk0.gaijinsmash.riderz.utils.LogUtil
import javax.inject.Inject

class HomeFragment : BaseFragment() {

    @Inject
    lateinit var homeViewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var permissionPresenter: PermissionPresenter
    private lateinit var estimatePresenter: HomeEtdPresenter
    private lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.initViewModel()
        savedInstanceState?.let { viewModel.restoreState(savedInstanceState) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        LogUtil.log(Log.DEBUG, TAG, "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        LogUtil.log(Log.DEBUG, TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        super.setTitle(activity, getString(R.string.app_name))
        super.expandAppBar(activity)
        super.toggleBottomNavigation(true) // always assume its closed
        initAppReviewCheck()
        initAdvisories(viewModel.bsaLiveData)
        initUserLocation(context)
        initLocationHandler(viewModel.isLocationPermissionEnabledLD)
        initNavigationObserver()
        LogUtil.log(Log.DEBUG, TAG, "onResume")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        // todo save states -
        super.onSaveInstanceState(outState)
    }

    override fun onPause() {
        LogUtil.log(Log.DEBUG, TAG, "onPause")
        super.onPause()
    }

    override fun onStop() {
        LogUtil.log(Log.DEBUG, TAG, "onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        LogUtil.log(Log.DEBUG, TAG, "onDestroyView")
        super.onDestroyView()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (permissions.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.isLocationPermissionEnabledLD.postValue(true)
                }
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    /**
     *  Helper Methods
     */
    private fun initViewModel() {
        viewModel = ViewModelProvider(this, homeViewModelFactory)[HomeViewModel::class.java]
    }

    private fun initAppReviewCheck() {
        // todo check if in-app review should be launched
    }

    /**
     * Fetches data for BART advisories - i.e. delay reports
     */
    private fun initAdvisories(bsa: LiveData<BsaXmlResponse>) {
        bsa.observe(
            viewLifecycleOwner,
        ) { bsaXmlResponse ->
            if (bsaXmlResponse != null) {
                binding.homeBsaRecyclerView.visibility = View.VISIBLE
                viewModel.bsaAdapter = BsaRecyclerAdapter(
                    bsaXmlResponse.bsaList
                        ?: mutableListOf(),
                    viewModel,
                )
                binding.homeBsaRecyclerView.adapter = viewModel.bsaAdapter
                binding.homeBsaRecyclerView.layoutManager = LinearLayoutManager(context)
            }
        }
    }

    private fun initUserLocation(context: Context?) {
        if (ContextCompat.checkSelfPermission(context as Context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            viewModel.isLocationPermissionEnabledLD.postValue(true)
        } else {
            if (LocationManager.checkIfExplanationIsNeeded(activity)) {
                LocationManager.showPermissionExplanation(activity)
            } else {
                LocationManager.requestPermissions(activity)
            }
        }
    }

    private fun initLocationHandler(permissionLiveData: LiveData<Boolean>) {
        permissionLiveData.observe(
            viewLifecycleOwner,
        ) { result ->
            if (result) {
                context?.let {
                    locationManager = LocationManager(it)
                    initEstimatesPresenter()
                }
            } else {
                LogUtil.logEvent("Permission Denied for Location")
            }
        }
    }

    private fun initEstimatesPresenter() {
        estimatePresenter = HomeEtdPresenter(binding.estimatePresenterLayout, viewModel, lifecycleScope)
        lifecycle.addObserver(estimatePresenter)
    }

    /**
     * Display permission cardView when Location Permissions is off.
     * TODO - under construction.
     */
    private fun loadPermissionView() {
        permissionPresenter = PermissionPresenter(activity, viewModel)
        permissionPresenter.showDialog()
    }

    private fun initNavigationObserver() {
        viewModel.navigationLiveData.observe(
            viewLifecycleOwner,
        ) { tag ->
            when (tag) {
                StationInfoFragment.TAG -> {
                    viewModel.navigationLiveData.postValue("")
                    val args = Bundle()
                    args.putString(StationInfoFragment.STATION_INFO_EXTRA, viewModel.closestStation?.abbr)
                    NavHostFragment.findNavController(this).navigate(
                        R.id.action_homeFragment_to_stationInfoFragment,
                        args,
                        null,
                        null,
                    )
                }

                else -> Logger.e("unhandled tag: $tag")
            }
        }
    }

    companion object {
        const val TAG = "HomeFragment"
    }
}
