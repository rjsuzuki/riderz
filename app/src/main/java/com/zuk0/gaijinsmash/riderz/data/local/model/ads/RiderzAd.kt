package com.zuk0.gaijinsmash.riderz.data.local.model.ads

import android.util.Log
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError

abstract class RiderzAd {

    abstract val name: String
    abstract val adUnitId: String
    private var adView: AdView? = null

    fun setAdView(adView: AdView?) {
        this.adView = adView
        adView?.adUnitId = adUnitId
        adView?.adListener = adListener
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)
    }

    private val adListener = object : AdListener() {
        override fun onAdClicked() {
            Log.d(name, "onAdClicked")
            // Code to be executed when the user clicks on an ad.
        }

        override fun onAdClosed() {
            Log.i(name, "onAdClosed")
            // Code to be executed when the user is about to return
            // to the app after tapping on an ad.
        }

        override fun onAdFailedToLoad(adError: LoadAdError) {
            Log.e(name, "onAdFailedToLoad")
            // Code to be executed when an ad request fails.
        }

        override fun onAdImpression() {
            Log.d(name, "onAdImpression")
            // Code to be executed when an impression is recorded
            // for an ad.
        }

        override fun onAdLoaded() {
            Log.d(name, "onAdLoaded")
            // Code to be executed when an ad finishes loading.
        }

        override fun onAdOpened() {
            Log.i(name, "onAdOpened")
            // Code to be executed when an ad opens an overlay that
            // covers the screen.
        }
    }

    companion object {
        const val TEST_BANNER_ID = "ca-app-pub-3940256099942544/6300978111"
        const val HOME_BANNER_ID = "ca-app-pub-2091123138998043/8486576090"
        const val TRIP_BANNER_ID = "ca-app-pub-2091123138998043/9407667875"
    }
}
