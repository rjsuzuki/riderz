package com.zuk0.gaijinsmash.riderz.di.module.activity.fragment

import dagger.Module

/**
 * place all fragment scoped injections here
 */
@Module
class FragmentModule
