package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.webview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.databinding.FragmentWebviewBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment

class WebviewFragment : BaseFragment() {

    private lateinit var binding: FragmentWebviewBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentWebviewBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.collapseAppBar(activity)
        super.setTitle(activity, getString(R.string.bart_site_title))

        binding.webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return false
            }
        }
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.settings.javaScriptCanOpenWindowsAutomatically = true
        binding.webview.loadUrl(BART_URL)
    }

    companion object {
        private const val TAG = "WebviewFragment"
        private const val BART_URL = "https://bart.gov"
    }
}
