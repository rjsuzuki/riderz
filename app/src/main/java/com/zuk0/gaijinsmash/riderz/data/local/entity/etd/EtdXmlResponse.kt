package com.zuk0.gaijinsmash.riderz.data.local.entity.etd

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.Station
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.sql.Timestamp

@Entity(tableName = "estimates")
@Root(name = "root", strict = false)
data class EtdXmlResponse
@JvmOverloads constructor(
    /*
      A Station object will have two ETDs
      one for  Northbound estimates and one for Southbound estimates
   */
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0,

    @SerializedName("station")
    @Expose
    @ColumnInfo
    @field:Element
    var station: Station? = null,

    @ColumnInfo
    var timestamp: Timestamp? = null,

    @ColumnInfo
    var originAbbr: String? = null,

)
