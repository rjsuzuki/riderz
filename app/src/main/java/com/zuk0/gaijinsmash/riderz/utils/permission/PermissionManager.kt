package com.zuk0.gaijinsmash.riderz.utils.permission

object PermissionManager {

    const val TAG = "PermissionManager"

    fun isPermissionGranted(permission: String): Boolean {
        return false
    }

    fun arePermissionsGranted(permissions: Array<String>): Boolean {
        return false
    }

    fun requestPermissions() {
    }

    fun showPermissionExplanation() {
    }
}
