package com.zuk0.gaijinsmash.riderz.data.local.entity.trip

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Uri {
    @SerializedName("#cdata-section")
    @Expose
    var cdataSection: String? = null
}
