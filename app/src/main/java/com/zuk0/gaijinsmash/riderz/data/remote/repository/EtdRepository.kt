package com.zuk0.gaijinsmash.riderz.data.remote.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonSyntaxException
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdJsonResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.EtdDao
import com.zuk0.gaijinsmash.riderz.data.remote.retrofit.BartService
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper.Companion.error
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper.Companion.success
import com.zuk0.gaijinsmash.riderz.utils.LogUtil
import com.zuk0.gaijinsmash.riderz.utils.TimeDateUtils
import retrofit2.Response
import java.io.IOException
import java.sql.Date
import java.sql.Timestamp
import java.util.concurrent.Executor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EtdRepository
@Inject constructor(val service: BartService, val etdDao: EtdDao, val executor: Executor) {

    fun getEtd(originAbbr: String?): LiveData<LiveDataWrapper<EtdXmlResponse>> {
        val data = MutableLiveData<LiveDataWrapper<EtdXmlResponse>>()
        executor.execute {
            var isCacheExpired = true
            originAbbr?.let {
                val cachedEtd = etdDao.getLatest(originAbbr)
                cachedEtd?.let {
                    Log.d(TAG, "Cache found: $it")
                    cachedEtd.timestamp?.time?.let { time ->
                        val cachedDate = Date(time)
                        val result = TimeDateUtils.durationOfMinutesBetweenDates(cachedDate, Date(System.currentTimeMillis()))
                        Log.d(TAG, "cachedDate: $cachedDate, result:$result < 5 ?")
                        if (result < MAX_MINUTES) {
                            // use cache
                            isCacheExpired = false
                            data.postValue(success(it) as LiveDataWrapper<EtdXmlResponse>)
                            LogUtil.log("Using cached ETD: $result < $MAX_MINUTES")
                        } else {
                            val list = etdDao.getAll(originAbbr)
                            list?.let { savedEtds ->
                                for (etd in savedEtds) {
                                    Log.d(TAG, "Deleting expired etd cache: ${etd.id}")
                                    etdDao.delete(etd)
                                }
                            }
                        }
                    }
                } ?: Log.w(TAG, "cached etd is null")

                if (isCacheExpired) {
                    LogUtil.log("Cache expired or null - fetching remotely")
                    try {
                        val response: Response<EtdJsonResponse> = service.getEtdJson(originAbbr).execute()
                        val etd = response.body()
                        etd?.let { res ->

                            val xmlResponse = EtdXmlResponse(
                                station = res.root?.station?.get(0),
                                timestamp = Timestamp(System.currentTimeMillis()),
                                originAbbr = originAbbr,
                            )

                            etdDao.save(xmlResponse)
                            Log.d(TAG, "creating cache of etd for origin: ${xmlResponse.originAbbr} at timestamp: ${xmlResponse.timestamp}")
                            Log.d(TAG, "ETD: $res")
                            data.postValue(success(xmlResponse) as LiveDataWrapper<EtdXmlResponse>)
                        }
                    } catch (e: IOException) {
                        data.postValue(error(it, "IOException") as LiveDataWrapper<EtdXmlResponse>)
                        LogUtil.logException(e)
                    } catch (e: JsonSyntaxException) {
                        data.postValue(error(it, "JsonSyntaxException") as LiveDataWrapper<EtdXmlResponse>)
                        LogUtil.logException(e)
                    } catch (e: Exception) {
                        data.postValue(error(it, "JsonSyntaxException") as LiveDataWrapper<EtdXmlResponse>)
                        LogUtil.logException(e)
                    }
                }
            }
        }
        return data
    }

    companion object {
        private const val TAG = "EtdRepository"
        private const val MAX_MINUTES = 5
    }
}
