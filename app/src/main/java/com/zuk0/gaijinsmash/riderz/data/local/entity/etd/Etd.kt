package com.zuk0.gaijinsmash.riderz.data.local.entity.etd

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList

class Etd {
    @SerializedName("destination")
    @Expose
    @field:Element(name = "destination")
    var destination: String? = null

    @SerializedName("abbreviation")
    @Expose
    @field:Element(name = "abbreviation")
    var destinationAbbr: String? = null

    @SerializedName("limited")
    @Expose
    @field:Element(name = "limited")
    var limited = 0

    @SerializedName("estimate")
    @Expose
    @field:ElementList(inline = true)
    var estimateList: MutableList<Estimate>? = null
}
