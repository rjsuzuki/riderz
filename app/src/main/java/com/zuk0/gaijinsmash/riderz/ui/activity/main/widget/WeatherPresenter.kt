package com.zuk0.gaijinsmash.riderz.ui.activity.main.widget

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import androidx.preference.PreferenceManager
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.weather.WeatherResponse
import com.zuk0.gaijinsmash.riderz.databinding.MainActivityBinding
import com.zuk0.gaijinsmash.riderz.databinding.PresenterWeatherBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.MainViewModel
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.settings.SettingsFragment
import com.zuk0.gaijinsmash.riderz.ui.shared.livedata.LiveDataWrapper
import com.zuk0.gaijinsmash.riderz.utils.LogUtil
import java.util.Locale
import kotlin.math.roundToLong

class WeatherPresenter(
    context: Context,
    val viewModel: MainViewModel,
    private val mainActivityBinding: MainActivityBinding,
) : LifecycleObserver, SharedPreferences.OnSharedPreferenceChangeListener {

    private val weatherBinding = PresenterWeatherBinding.inflate(LayoutInflater.from(context))

    private val weatherObserver = Observer<LiveDataWrapper<WeatherResponse>> { response ->
        when (response.status) {
            LiveDataWrapper.Status.SUCCESS -> {
                Logger.i("success")
                displayWeather(response.data)
            }
            LiveDataWrapper.Status.ERROR -> {
                response.msg?.let {
                    LogUtil.log(it)
                }
            }
            else -> {
                Logger.e("WEATHER ERROR")
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        LogUtil.log("onStart")
        viewModel.getLocation() // required for weather
        initView()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        LogUtil.log("onResume")
        // todo restore state
        val prefs = PreferenceManager.getDefaultSharedPreferences(viewModel.getApplication())
        prefs.registerOnSharedPreferenceChangeListener(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        LogUtil.log("onPause")
        // todo save state
        viewModel.getWeather().removeObserver(weatherObserver)
        val prefs = PreferenceManager.getDefaultSharedPreferences(viewModel.getApplication())
        prefs.unregisterOnSharedPreferenceChangeListener(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        LogUtil.log("onStop")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        LogUtil.log("onDestroy")
    }

    private fun initView() {
        Logger.i("initializing weather")
        val container = mainActivityBinding.root.findViewById<FrameLayout>(R.id.widget_weather_container)
        container?.removeView(weatherBinding.root)
        container?.addView(weatherBinding.root)
        viewModel.getWeather().observeForever(weatherObserver)
    }

    /**
     * @param isMetric
     */
    private fun refreshWind(isMetric: Boolean) {
        if (isMetric) {
            var speed = weatherBinding.weatherWindTv.text.toString().toDouble()
            speed = viewModel.toKPH(speed)
            weatherBinding.weatherWindMetricTv.text = viewModel.getApplication<Application>().getString(R.string.kph)
        } else {
            var speed = weatherBinding.weatherWindTv.text.toString().toDouble()
            speed = viewModel.toMPH(speed)
            weatherBinding.weatherWindMetricTv.text = viewModel.getApplication<Application>().getString(R.string.mph)
        }
    }

    /**
     * @param isMetric
     */
    private fun refreshTemperature(isMetric: Boolean) {
        if (isMetric) {
            val c = weatherBinding.weatherTempTv.text.toString().toDouble()
            val cTemp = viewModel.fahrenheightToCelcius(c)
            weatherBinding.weatherTempTv.text = viewModel.formatDoubleValue(cTemp)
            weatherBinding.weatherTempIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_celcius, weatherBinding.root.context.theme))
        } else {
            val f = weatherBinding.weatherTempTv.text.toString().toDouble()
            val fTemp = viewModel.celciusToFahrenheit(f)
            weatherBinding.weatherTempTv.text = viewModel.formatDoubleValue(fTemp)
            weatherBinding.weatherTempIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_fahrenheit, weatherBinding.root.context.theme))
        }
    }

    /**
     * Initialize the UI for the weather widget
     * todo need to refactor this - DRY!
     * @param response - @see [WeatherResponse]
     */
    private fun displayWeather(response: WeatherResponse?) {
        Logger.i("displaying weather")
        response ?: return
        var textColor = ResourcesCompat.getColor(weatherBinding.root.context.resources, R.color.white, weatherBinding.root.context.theme) // default for daytime

        val weatherCondition = response.weather?.get(0)?.main ?: ""
        when {
            viewModel.isDuskOrDawn -> {
                when (weatherCondition.lowercase(Locale.getDefault())) {
                    "clear" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_half_sun, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_clear_icon_content_desc)
                    }
                    "clouds" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_cloud, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_cloud_icon_content_desc)
                    }
                    "rain" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_rain, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_rain_icon_content_desc)
                    }
                    "wind" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_wind, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_wind_icon_content_desc)
                    }
                    else -> {
                        LogUtil.log("unhandled weather type: $weatherCondition")
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_half_sun, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_clear_icon_content_desc)
                    }
                }
            }
            viewModel.isDaytime -> {
                when (weatherCondition.lowercase(Locale.getDefault())) {
                    "clear" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_sun, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_clear_icon_content_desc)
                    }
                    "clouds" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_cloudy_sun, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_cloud_icon_content_desc)
                    }
                    "rain" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_rain, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_rain_icon_content_desc)
                    }
                    "wind" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_wind, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_wind_icon_content_desc)
                    }
                    else -> {
                        LogUtil.log("unhandled weather type: $weatherCondition")
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_sun, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_clear_icon_content_desc)
                    }
                }
            }
            else -> {
                textColor = ResourcesCompat.getColor(weatherBinding.root.context.resources, R.color.white, weatherBinding.root.context.theme)
                when (weatherCondition.lowercase(Locale.getDefault())) {
                    "clear" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_moon, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_clear_icon_content_desc)
                    }
                    "clouds" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_cloudy_moon, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_cloud_icon_content_desc)
                    }
                    "rain" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_rain, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_rain_icon_content_desc)
                    }
                    "wind" -> {
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_wind, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_wind_icon_content_desc)
                    }
                    else -> {
                        LogUtil.log("unhandled weather type: $weatherCondition")
                        weatherBinding.weatherIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_moon, weatherBinding.root.context.theme))
                        weatherBinding.weatherIcon.contentDescription = viewModel.getApplication<Application>().getString(R.string.weather_clear_icon_content_desc)
                    }
                }
            }
        }

        if (response.name != null) { // city
            weatherBinding.weatherNameTv.text = response.name
            weatherBinding.weatherNameTv.setTextColor(textColor)
        }

        if (response.main != null) {
            if (response.main?.temp != null) {
                // ° F = 9/5 (K - 273) + 32
                val imperialTemp = viewModel.kelvinToFahrenheit(response.main?.temp as Double).roundToLong().toString()

                val metricTemp = viewModel.kelvinToCelcius(response.main?.temp as Double).roundToLong().toString()
                Log.d(TAG, "imperial: $imperialTemp, metric: $metricTemp")

                if (viewModel.isMetricSystemEnabled()) {
                    weatherBinding.weatherTempTv.text = metricTemp
                    weatherBinding.weatherTempTv.setTextColor(textColor) // abstract
                    weatherBinding.weatherTempIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_celcius, weatherBinding.root.context.theme))
                } else {
                    weatherBinding.weatherTempTv.text = imperialTemp
                    weatherBinding.weatherTempTv.setTextColor(textColor) // abstract
                    weatherBinding.weatherTempIcon.setImageDrawable(ResourcesCompat.getDrawable(weatherBinding.root.context.resources, R.drawable.ic_weather_fahrenheit, weatherBinding.root.context.theme))
                }
            }
        }

        if (response.main?.humidity != null) {
            val humidity = "${response.main?.humidity}%"
            weatherBinding.weatherHumidityTv.text = humidity // todo use string resource
            weatherBinding.weatherHumidityTv.setTextColor(textColor)
        }

        if (response.wind != null) {
            if (viewModel.isMetricSystemEnabled()) {
                val wind = viewModel.formatDoubleValue(viewModel.toKPH(response.wind?.speed))
                weatherBinding.weatherWindTv.text = wind
                weatherBinding.weatherWindTv.setTextColor(textColor)
                weatherBinding.weatherWindMetricTv.text = viewModel.getApplication<Application>().getString(R.string.kph)
                weatherBinding.weatherWindMetricTv.setTextColor(textColor)
            } else {
                val wind = viewModel.formatDoubleValue(response.wind?.speed ?: 0.0)
                weatherBinding.weatherWindTv.text = wind
                weatherBinding.weatherWindTv.setTextColor(textColor)
                weatherBinding.weatherWindMetricTv.text = viewModel.getApplication<Application>().getString(R.string.mph)
                weatherBinding.weatherWindMetricTv.setTextColor(textColor)
            }
        }
    }

    companion object {
        private const val TAG = "MainWeatherPresenter"
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == SettingsFragment.PreferenceKey.METRIC_PREFS.value) {
            val result = sharedPreferences?.getBoolean(key, false)
            refreshTemperature(result ?: false)
            refreshWind(result ?: false)
        }
    }
}
