package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stations

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.databinding.FragmentStationsBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.BaseFragment
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.stationinfo.StationInfoFragment
import javax.inject.Inject

class StationsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentStationsBinding
    private lateinit var viewModel: StationsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stations, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        initStationList()
        super.collapseAppBar(activity)
        super.setTitle(activity, getString(R.string.stations_title))
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(StationsViewModel::class.java)
    }

    private fun handleListItemClick(view: View) {
        val bundle = initBundle(view)
        launchStationInfoFragment(bundle)
    }

    private fun initBundle(view: View): Bundle {
        val stationAbbr = (view.findViewById<View>(R.id.stationAbbr_textView) as TextView).text.toString()
        val bundle = Bundle()
        bundle.putString(StationInfoFragment.STATION_INFO_EXTRA, stationAbbr) // todo: convert StationAbbr to enum
        return bundle
    }

    private fun launchStationInfoFragment(bundle: Bundle) {
        NavHostFragment.findNavController(this).navigate(
            R.id.action_stationsFragment_to_stationInfoFragment,
            bundle,
            null,
            null,
        )
    }

    private fun initStationList() {
        viewModel.getListFromDb(activity)?.observe(
            viewLifecycleOwner,
            Observer { stations ->
                // update the ui
                if (stations != null) {
                    val adapter = StationRecyclerAdapter(stations)
                    binding.stationRecyclerView.adapter = adapter
                    binding.stationRecyclerView.layoutManager = LinearLayoutManager(activity)
                    adapter.setClickListener(
                        View.OnClickListener {
                            handleListItemClick(it)
                        },
                    )
                } else {
                    viewModel.listFromRepo
                        .observe(
                            viewLifecycleOwner,
                            Observer { data ->
                                val adapter: StationRecyclerAdapter
                                if (data != null) {
                                    adapter = StationRecyclerAdapter(data.stationList)
                                    binding.stationRecyclerView.adapter = adapter
                                    binding.stationRecyclerView.layoutManager = LinearLayoutManager(activity)
                                    adapter.setClickListener(
                                        View.OnClickListener {
                                            handleListItemClick(it)
                                        },
                                    )
                                } else {
                                    Log.wtf("StationsFragment", "error with list")
                                }
                            },
                        )
                }
                binding.stationProgressBar.visibility = View.GONE
            },
        )
    }
}
