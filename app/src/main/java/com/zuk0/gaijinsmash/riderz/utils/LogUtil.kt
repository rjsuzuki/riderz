package com.zuk0.gaijinsmash.riderz.utils

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.orhanobut.logger.Logger
import com.zuk0.gaijinsmash.riderz.BuildConfig

object LogUtil {

    private const val ENABLE = false

    /**
     * A convenient wrapper for crashlytics logger to check for Fabric initialization first
     * @param logType: Int (ERROR, VERBOSE, DEBUG, etc)
     * @param tag: String
     * @param msg : String
     */
    fun log(logType: Int, tag: String, msg: String) {
        when (logType) {
            Log.INFO -> Log.i(tag, msg)
            Log.VERBOSE -> Log.v(tag, msg)
            Log.DEBUG -> Log.d(tag, msg)
            Log.WARN -> Log.w(tag, msg)
            Log.ERROR -> Log.e(tag, msg)
            else -> Log.wtf(tag, msg)
        }
        try {
            FirebaseCrashlytics.getInstance().log("$tag : $msg")
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    /**
     * A convenient wrapper for crashlytics logger to check for Fabric initialization first
     * @param msg : String
     */
    fun log(msg: String?) {
        if (!msg.isNullOrEmpty()) {
            FirebaseCrashlytics.getInstance().log(msg)
        }
        if (BuildConfig.DEBUG) {
            Logger.d(msg)
        }
    }

    /**
     * A convenient wrapper for crashlytics logger to check for Fabric initialization first
     * @param t : Throwable
     */
    fun logException(t: Throwable) {
        if (BuildConfig.DEBUG) Logger.e(t.toString())
        FirebaseCrashlytics.getInstance().log(t.message ?: "")
    }

    /**
     * Log an event to crashlytics
     * this type will show regardless of a crash once the user restarts the app
     */
    fun logEvent(msg: String) {
        if (ENABLE) {
            FirebaseCrashlytics.getInstance().log(msg)
            Logger.d(msg)
        }
    }
}
