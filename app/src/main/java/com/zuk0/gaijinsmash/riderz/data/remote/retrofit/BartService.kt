package com.zuk0.gaijinsmash.riderz.data.remote.retrofit

import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.BsaJsonResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.bsa.BsaXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdJsonResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.EtdXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.station.StationXmlResponse
import com.zuk0.gaijinsmash.riderz.data.local.entity.trip.TripJsonResponse
import com.zuk0.gaijinsmash.riderz.data.remote.retrofit.ResponseTypeConverter.Json
import com.zuk0.gaijinsmash.riderz.data.remote.retrofit.ResponseTypeConverter.Xml
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface BartService {

    @Json
    @GET("sched.aspx?cmd=depart&b=0&a=4&json=y")
    fun getTripJson(
        @Query("orig") origin: String?,
        @Query("dest") destination: String?,
        @Query("date") date: String?,
        @Query("time") time: String?,
    ): Call<TripJsonResponse>

    @Xml
    @GET("etd.aspx?cmd=etd")
    fun getEtd(@Query("orig") origin: String?): Call<EtdXmlResponse>

    @Json
    @GET("etd.aspx?cmd=etd&json=y")
    fun getEtdJson(@Query("orig") origin: String?): Call<EtdJsonResponse>

    @Xml
    @GET("bsa.aspx?cmd=bsa")
    fun getBsa(): Call<BsaXmlResponse>

    @Json
    @GET("bsa.aspx?cmd=bsa&json=y")
    fun getBsaJson(): Call<BsaJsonResponse>

    // todo: test this
    @Xml
    @GET("stn.aspx?cmd=stns")
    fun getAllStations(): Call<StationXmlResponse>

    // todo: test this
    @Xml
    @GET("stn.aspx?cmd=stninfo")
    fun getStation(@Query("orig") originAbbr: String): Call<StationXmlResponse>
}
