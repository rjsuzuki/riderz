package com.zuk0.gaijinsmash.riderz.di.module

import android.app.Application
import android.content.Context
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.BsaDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.EtdDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.FavoriteDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.StationDao
import com.zuk0.gaijinsmash.riderz.data.local.room.dao.TripDao
import com.zuk0.gaijinsmash.riderz.data.local.room.database.RiderzDatabase
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/*
   This module will provide the dependencies for the overall application level
   This module will provide the dependencies for the overall application level
   @Singleton is application scope
*/
@Module
class AppModule {

    @Provides
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    fun provideDatabase(context: Context): RiderzDatabase {
        return RiderzDatabase.getRoomDb(context)
    }

    @Provides
    fun provideFavoritesDao(db: RiderzDatabase): FavoriteDao {
        return db.favoriteDao()
    }

    @Provides
    fun provideStationDao(db: RiderzDatabase): StationDao {
        return db.stationDao()
    }

    @Provides
    fun provideBsaDao(db: RiderzDatabase): BsaDao {
        return db.bsaDao()
    }

    @Provides
    fun provideExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    @Provides
    fun provideEtdDao(db: RiderzDatabase): EtdDao {
        return db.etdDao()
    }

    @Provides
    fun provideTripDao(db: RiderzDatabase): TripDao {
        return db.tripDao()
    }
}
