package com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.adapter.etd

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.zuk0.gaijinsmash.riderz.R
import com.zuk0.gaijinsmash.riderz.data.local.entity.etd.Estimate
import com.zuk0.gaijinsmash.riderz.databinding.ListColEstimateBinding
import com.zuk0.gaijinsmash.riderz.ui.activity.main.fragment.home.HomeViewModel
import com.zuk0.gaijinsmash.riderz.utils.BartRoutesUtils
import com.zuk0.gaijinsmash.riderz.utils.LogUtil
import com.zuk0.gaijinsmash.riderz.utils.ThemeUtil
import com.zuk0.gaijinsmash.riderz.utils.TrainTimer

class EstimateRecyclerAdapter(
    var estimateList: MutableList<Estimate>,
    val viewModel: HomeViewModel,
) : Adapter<EstimateRecyclerAdapter.ViewHolder>() {

    class ViewHolder(val binding: ListColEstimateBinding) : RecyclerView.ViewHolder(binding.root)

    private var notifyUIChangeListener: NotifyUIChange? = null
    private lateinit var binding: ListColEstimateBinding
    private lateinit var recyclerView: RecyclerView
    private val handler by lazy {
        Handler(Looper.getMainLooper())
    }

    fun setNotifyUIChange(listener: NotifyUIChange) {
        this.notifyUIChangeListener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return ETD_VIEW_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = ListColEstimateBinding.inflate(LayoutInflater.from(parent.context))
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val estimate = estimateList[position]

        // set car length of train
        val carLength = estimate.length.toString()
        holder.binding.estimateResultCarTv.text = carLength

        // set time remaining
        estimate.minutes?.toIntOrNull()?.let {
            val calculatedMinutes = it - viewModel.cachedTimeDifference
            Log.d(TAG, "uncalculated minutes: ${estimate.minutes}, calculated minutes: $calculatedMinutes")
            holder.binding.estimateResultMinutesTv.text = calculatedMinutes.toString()
        }

        val minutes: String? = estimate.minutes
        if (!minutes.equals("Leaving", ignoreCase = true)) {
            viewModel.timer.timeStampOfUpdateLiveData.observeForever(getTimeObserver(holder, position))
        } else {
            holder.binding.estimateResultMinutesTv.text = "0"
            removeEstimate(position)
        }

        BartRoutesUtils.setLineBarByColor(holder.binding.background.context, estimate.color, holder.binding.estimateResultTrainIcon)
    }

    private fun getTimeObserver(holder: ViewHolder, position: Int): Observer<Long> {
        return Observer { amountOfTimePassed ->

            if (amountOfTimePassed == TrainTimer.TIMER_FINISHED) {
                viewModel.isEtdRefreshNeeded.postValue(true)
                return@Observer
            }

            val remainingMinutes = holder.binding.estimateResultMinutesTv.text.toString().toInt()
            if (remainingMinutes <= 0) {
                removeEstimate(position)
            } else {
                val updatedTime = remainingMinutes - 1
                val msg = "$updatedTime"

                val textAnimation = AlphaAnimation(1.0f, 1.0f)
                textAnimation.duration = 500
                textAnimation.repeatCount = 1
                textAnimation.repeatMode = Animation.REVERSE
                textAnimation.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationRepeat(animation: Animation?) {
                        holder.binding.estimateResultMinutesTv.textSize = 16F
                        holder.binding.estimateResultMinutesTv.setTextColor(ResourcesCompat.getColor(viewModel.getApplication<Application>().resources, R.color.colorSecondaryVariant, viewModel.getApplication<Application>().theme))
                        holder.binding.estimateResultMinutesTv.text = msg
                    }

                    override fun onAnimationEnd(animation: Animation?) {
                        holder.binding.estimateResultMinutesTv.textSize = 14F
                        if (ThemeUtil.isDarkThemeOn(holder.binding.root.context)) {
                            holder.binding.estimateResultMinutesTv.setTextColor(ResourcesCompat.getColor(viewModel.getApplication<Application>().resources, R.color.off_white, viewModel.getApplication<Application>().theme))
                        } else {
                            holder.binding.estimateResultMinutesTv.setTextColor(ResourcesCompat.getColor(viewModel.getApplication<Application>().resources, R.color.black, viewModel.getApplication<Application>().theme))
                        }
                    }

                    override fun onAnimationStart(animation: Animation?) {
                    }
                })
                holder.binding.estimateResultMinutesTv.startAnimation(textAnimation)
            }
        }
    }

    @Synchronized
    private fun removeEstimate(position: Int) {
        val runnable = Runnable {
            if (estimateList.isNullOrEmpty()) {
                Log.d(TAG, "Nothing to remove at position $position - estimate list is null or empty")
                notifyUIChangeListener?.onEmpty()
            } else {
                val newList = mutableListOf<Estimate>()
                newList.addAll(estimateList)
                newList.removeAt(position)
                update(newList)
            }
        }
        handler.post(runnable)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        Log.i("recycler", "onDetachedFromRecyclerView")
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        Log.d("onDetachedFromView", "timers cleared")
    }

    override fun getItemCount(): Int {
        return estimateList.size
    }

    fun update(newList: List<Estimate>) {
        try {
            val diffCallback = EstimateDiffCallback(estimateList, newList)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            estimateList.clear()
            estimateList.addAll(newList)
            diffResult.dispatchUpdatesTo(this)
        } catch (e: Exception) {
            LogUtil.logException(e)
        }
    }

    companion object {
        private const val TAG = "EstimateRecAdapter"
        private const val TITLE_VIEW_TYPE = 1000
        private const val ETD_VIEW_TYPE = 2000
        private const val FAV_VIEW_TYPE = 2000
    }

    interface NotifyUIChange {
        fun onEmpty()
    }
}
