# Riderz 

 🚇 a transportation app for BART riders


## Privacy Policy

Regarding User Data:

This app uses Firebase Crashlytics and Performance - Quality services by Google's Firebase

- Crashlytics is used for the collection of anonymous user data in order to debug and analyze bugs
  and crashes that are
  directly related to this app.
- Performance uses anonymous data to analyze the performance of the application on a specific
  device.
- Firebase Crashlytics retains crash stack traces, extracted minidump data, and associated
  identifiers (including Instance IDs) for 90 days.
- Data sent to Firebase neither stores or transfers to other services or resources, nor associated
  data with other data available to Google.
- For more information you can visit Google's Privacy and Security Information pages:
- [How Crashlytics Uses Collected Data](https://firebase.google.com/terms/crashlytics-app-distribution-data-processing-terms)

This app uses Google AdMob to show ads in our free app.

- Data for advertising and analytics purposes only, so we can provide a better app
- AdMob collects data and uses a unique resettable identifier on your device to show relevant ads to
  you.
- Users can opt-out of personalized advertising by visiting their devices Ads Settings.
- For GDPR, users from the European Union will see a message asking for their consent
- For CCPA, California users will see a message asking for their consent.
- [What is AdMob](https://admob.google.com/home/resources/what-is-admob/)

## Permissions:

1. Location Services (Fine/Coarse) the best experience is with location services enabled while the
   app is in use,
   in order to provide Real-Time estimates of the closest BART station for the user.
2. Internet - for the ability to use REST services for BART and Weather services.
3. Network State - for monitoring and adjusting our api requests appropriately
4. Storage (sqlite3) - we request to the permission to create an sqlite database on the device in
   order
   to properly store BART information locally to help ensure the app can still provide usefulness
   even without a network connection.
5. Google Advertising ID - we use the google advertising id to work with Google AdMob.

--------------------------------------------------------------------------------

## Screenshots

![home_night](https://i.imgur.com/nyBEf0Pm.png)
![home_day](https://i.imgur.com/qHOykRLm.png)
![nav](https://i.imgur.com/QRMdt7mm.png)
![fab](https://i.imgur.com/ee8URSAm.png)
![trip](https://i.imgur.com/2vu1pHfm.png)
![results](https://i.imgur.com/oZOP63tm.png)
![favorites](https://i.imgur.com/SeacQugm.png)
![gmap](https://i.imgur.com/Jt7od97m.png)
![bartmap](https://i.imgur.com/sEL3HuXm.png)

 
 
 

## Copyright
Copyright 2018 ryanjsuzuki.com
Refer to the LICENSE.md file for licensing details.
