
Hello!

This response is just to let you know that your message has been received and we'll get back to you as soon as possible. 

For your reference:
 
Issue #: %{ISSUE_ID}

Cheers,

Riderz Support Crew
